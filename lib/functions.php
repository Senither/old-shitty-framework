<?php
defined('LIB_START') or exit('No direct script access allowed');

$_SERVER['MINIMIZE'] = MINIMIZE;
register_shutdown_function(function () {
    if (!isset($_SERVER['MINIMIZE'])) {
        return false;
    }

    $minimize = $_SERVER['MINIMIZE'];

    if (!is_bool($minimize)) {
        return false;
    }

    if ($minimize) {
        $content = ob_get_contents();

        if (mb_strlen($content) > 0) {
            ob_end_clean();
        }

        echo implode('', array_map("trim", explode("\n", $content)));
    }
    return true;
});

if (!function_exists('lib_path')) {

    /**
     * Gets the library path
     * @return String The library path
     */
    function lib_path()
    {
        return __DIR__;
    }
}

if (!function_exists('root_path')) {

    /**
     * Gets the root path
     * @return String The root path
     */
    function root_path()
    {
        return dirname(lib_path());
    }
}

if (!function_exists('storage_path')) {

    /**
     * Gets the storage path
     * @return String The storage path
     */
    function storage_path()
    {
        return root_path() . DIRECTORY_SEPARATOR . 'storage';
    }
}

if (!function_exists('load_time')) {

    /**
     * Gets the pages current load time
     * @return String The current time it took for the page to load
     */
    function load_time($length = 3)
    {
        return number_format((microtime(true) - LIB_START), $length);
    }
}

if (!function_exists('lib_settings')) {

    /**
     * Returns an array of all the settings from the library
     * @return Array The library settings array
     */
    function lib_settings()
    {
        return [
            'start'        => LIB_START,
            'env'        => [
                'id'        => (ENVIRONMENT == 'development') ? 0 : (ENVIRONMENT == 'testing') ? 1 : 2,
                'name'        => ENVIRONMENT
            ]
        ];
    }
}

if (!function_exists('env')) {

    /**
     * Checks if a constant is defined or not, if it isn't defined
     * the function will define the constant for you with the given
     * fallback value.
     *
     * @param  Object $name     The constant to check against
     * @param  Object $fallback Fallback value if the constant doesn't exists
     * @return Object           The constant value
     */
    function env($name, $fallback)
    {
        defined($name) or define($name, $fallback);
        return constant($name);
    }
}

if (!function_exists('d')) {

    /**
     * Dumps all of the variables and objects that are parsed to the function.
     * The method of dumping the objects will differ depending on the current
     * enviorment, development enviorments will use Kint which may be heavy on
     * the page load speed, but looks awesome.
     *
     * @param  Object $dump This is really just a placeholder, you can parse in
     *                      as many arguments you want.
     */
    function d($dump)
    {
        switch (ENVIRONMENT) {
            case 'development':
                if (!class_exists('Kint')) {
                    require_once __DIR__ . '/Components/Kint/Kint.class.php';
                }

                foreach (func_get_args() as $arg) {
                    Kint::dump($arg);
                }
                break;
            
            case 'testing':
                foreach (func_get_args() as $arg) {
                    var_dump($arg);
                }
                break;
        }
    }
}

if (!function_exists('dd')) {

    /**
     * Will call our d(ump) function above and kill the page afterwords if
     * the enviorment isn't set as 'production'.
     *
     * @param  Object $dump This is really just a placeholder, you can parse in
     *                      as many arguments you want.
     */
    function dd($dump)
    {
        array_map('d', func_get_args());
        if (ENVIRONMENT != 'production') {
            exit;
        }
    }
}
