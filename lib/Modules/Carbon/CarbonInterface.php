<?php
defined('LIB_START') or exit('No direct script access allowed');

interface CarbonInterface
{

    // Initializing methods

    public static function now();

    public static function tomorrow();

    public static function yesterday();

    public static function createFrom($time);

    // Modifiers - Seconds

    public function addSecond();
    public function subSecond();

    public function addSeconds($seconds = 1);
    public function subSeconds($seconds = 1);

    // Modifiers - Minutes

    public function addMinute();
    public function subMinute();

    public function addMinutes($minutes = 1);
    public function subMinutes($minutes = 1);

    // Modifiers - Hours

    public function addHour();
    public function subHour();

    public function addHours($hours = 1);
    public function subHours($hours = 1);

    // Modifiers - Days

    public function addDay();
    public function subDay();

    public function addDays($days = 1);
    public function subDays($days = 1);

    // Modifiers - Week

    public function addWeek();
    public function subWeek();

    public function addWeeks($week = 1);
    public function subWeeks($week = 1);

    // Modifiers - Month

    public function addMonth();
    public function subMonth();

    public function addMonths($month = 1);
    public function subMonths($Month = 1);

    // Modifiers - Year

    public function addYear();
    public function subYear();

    public function addYears($year = 1);
    public function subYears($year = 1);

    // Boolean checks

    public function isWeekend();

    public function isYesterday();

    public function isToday();

    public function isTomorrow();

    public function isFuture();

    public function isPast();

    public function isLeapYear();

    public function isSameDay($time);

    public function isDay($day);

    public function isMonth($day);

    // Day

    public function getDay();

    public function getDayNumeric();

    // Week

    public function getWeekNumeric();

    // Month

    public function getMonth();

    public function getMonthNumeric();

    // Year

    public function getYear();

    // Timestamp

    public function getTimestamp();
}
