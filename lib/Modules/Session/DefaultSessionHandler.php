<?php
defined('LIB_START') or exit('No direct script access allowed');

class DefaultSessionHandler extends SessionHandler implements SessionInterface
{
    public function __construct()
    {
        if (session_id() == '') {
            // Starts our session.
            session_start();

            mt_rand(0, 4) === 0 ? $this->regenerate() : true; // 1/5
        }
    }

    public function put($name, $value, $carbon = null)
    {
        $parsed = explode('.', $name);

        $session =& $_SESSION;

        while (count($parsed) > 1) {
            $next = array_shift($parsed);

            if (! isset($session[$next]) || ! is_array($session[$next])) {
                $session[$next] = [];
            }

            $session =& $session[$next];
        }

        $session[array_shift($parsed)] = $value;
    }

    public function push($name, $value)
    {
        $array = $this->get($name, array());
        if (!is_array($array)) {
            return false;
        }

        $array[] = $value;

        $this->put($name, $array);
    }

    public function get($name, $fallback = null)
    {
        $parsed = explode('.', $name);

        $result = $_SESSION;

        while ($parsed) {
            $next = array_shift($parsed);

            if (isset($result[$next])) {
                $result = $result[$next];
            } else {
                if (is_callable($fallback)) {
                    return call_user_func_array($fallback, []);
                } else {
                    return $fallback;
                }
            }
        }

        return $result;
    }

    public function has($name)
    {
        $parsed = explode('.', $name);
        $result = $_SESSION;

        while ($parsed) {
            $next = array_shift($parsed);

            if (isset($result[$next])) {
                $result = $result[$next];
            } else {
                return false;
            }
        }

        return true;
    }

    public function forget($name)
    {
        $parsed = explode('.', $name);

        $session =& $_SESSION;
        $length = count($parsed) - 1;
        foreach ($parsed as $index => $next) {
            if ($index == $length) {
                if (isset($session[$next])) {
                    unset($session[$next]);
                }
                break;
            }

            if (isset($session[$next])) {
                $session =& $session[$next];
            }
        }
    }

    public function flush()
    {
        $_SESSION = [];
        return true;
    }

    public function all()
    {
        return $_SESSION;
    }

    public function regenerate()
    {
        return session_regenerate_id(true);
    }
}
