<?php
defined('LIB_START') or exit('No direct script access allowed');

interface SessionInterface extends LombokProvider
{
    public function push($name, $value);

    public function has($name);

    public function forget($name);

    public function flush();

    public function all();

    public function regenerate();
}
