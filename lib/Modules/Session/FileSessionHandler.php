<?php
defined('LIB_START') or exit('No direct script access allowed');

class FileSessionHandler extends SessionHandler implements SessionInterface
{
    protected $token;
    protected $name;
    protected $cookie;

    protected static $instance;

    private function setup()
    {
        $this->token = Config::get('session.token');
        $this->name = Config::get('session.cookie', 'sen_session');

        $this->cookie = [
            'lifetime' => Config::get('session.lifetime'),
            'path'     => Config::get('session.path', ini_get('session.cookie_path')),
            'domain'   => Config::get('session.domain', ini_get('session.cookie_domain')),
            'secure'   => isset($_SERVER['HTTPS']),
            'httponly' => !Config::get('session.secure', false)
        ];

        ini_set('session.use_cookies', 1);
        ini_set('session.use_only_cookies', 1);

        session_name($this->name);

        session_set_cookie_params(
            ($this->cookie['lifetime'] * 60),
            $this->cookie['path'],
            (is_string($this->cookie['domain']) ? $this->cookie['domain'] : null),
            $this->cookie['secure'],
            $this->cookie['httponly']
        );
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self;
            self::$instance->setup();
        }
        return self::$instance;
    }

    public function start()
    {
        if (session_id() === '') {
            if (session_start()) {
                return mt_rand(0, 4) === 0 ? $this->regenerate() : true; // 1/5
            }
        }

        return false;
    }

    public function flush()
    {
        if (session_id() === '') {
            return false;
        }

        $_SESSION = [];

        setcookie(
            $this->name,
            '',
            time() - 42000,
            $this->cookie['path'],
            $this->cookie['domain'],
            $this->cookie['secure'],
            $this->cookie['httponly']
        );

        return session_destroy();
    }

    public function regenerate()
    {
        return session_regenerate_id(true);
    }

    public function read($id)
    {
        return mcrypt_decrypt(MCRYPT_3DES, $this->token, parent::read($id), MCRYPT_MODE_ECB);
    }

    public function write($id, $data)
    {
        return parent::write($id, mcrypt_encrypt(MCRYPT_3DES, $this->token, $data, MCRYPT_MODE_ECB));
    }

    public function isExpired($ttl = 30)
    {
        $last = isset($_SESSION['_last_activity'])
            ? $_SESSION['_last_activity']
            : false;

        if ($last !== false && time() - $last > $ttl * 60) {
            return true;
        }

        $_SESSION['_last_activity'] = time();

        return false;
    }

    public function isFingerprint()
    {
        $hash = md5(
            $_SERVER['HTTP_USER_AGENT'] .
            (ip2long($_SERVER['REMOTE_ADDR']) & ip2long('255.255.0.0'))
        );

        if (isset($_SESSION['_fingerprint'])) {
            return $_SESSION['_fingerprint'] === $hash;
        }

        $_SESSION['_fingerprint'] = $hash;

        return true;
    }
    public function isValid()
    {
        return !$this->isExpired() && $this->isFingerprint();
    }

    public function get($name, $fallback = null)
    {
        $parsed = explode('.', $name);

        $result = $_SESSION;

        while ($parsed) {
            $next = array_shift($parsed);

            if (isset($result[$next])) {
                $result = $result[$next];
            } else {
                if (is_callable($fallback)) {
                    return call_user_func_array($fallback, []);
                } else {
                    return $fallback;
                }
            }
        }

        return $result;
    }

    public function put($name, $value, $carbon)
    {
        $parsed = explode('.', $name);

        $session =& $_SESSION;

        while (count($parsed) > 1) {
            $next = array_shift($parsed);

            if (! isset($session[$next]) || ! is_array($session[$next])) {
                $session[$next] = [];
            }

            $session =& $session[$next];
        }

        $session[array_shift($parsed)] = $value;
    }

    public function push($name, $value)
    {
        $array = $this->get($name, array());
        if (!is_array($array)) {
            return false;
        }

        $array[] = $value;

        $this->put($name, $array);
    }

    public function has($name)
    {
        $parsed = explode('.', $name);

        $result = $_SESSION;

        while ($parsed) {
            $next = array_shift($parsed);

            if (isset($result[$next])) {
                $result = $result[$next];
            } else {
                return false;
            }
        }
        
        return true;
    }

    public function forget($name)
    {
        $parsed = explode('.', $name);

        $session =& $_SESSION;
        $length = count($parsed) - 1;
        foreach ($parsed as $index => $next) {
            if ($index == $length) {
                if (isset($session[$next])) {
                    unset($session[$next]);
                }
                break;
            }

            if (isset($session[$next])) {
                $session =& $session[$next];
            }
        }
    }

    public function clear()
    {
        $_SESSION = [];
        return true;
    }

    public function all()
    {
        return $_SESSION;
    }
}

ini_set('session.save_handler', 'files');
session_set_save_handler(FileSessionHandler::getInstance(), true);
session_save_path(storage_path() . '/sessions');

FileSessionHandler::getInstance()->start();
