<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'SessionInterface.php';

class Session extends ModuleProvider
{
    private static $driver = null;

    public static function load($drive = null)
    {
        switch (($drive == null) ? Config::get('session.driver') : $drive) {
            case 'file':
                require_once 'FileSessionHandler.php';
                self::$driver = new FileSessionHandler;
                break;
            
            default:
                require_once 'DefaultSessionHandler.php';
                self::$driver = new DefaultSessionHandler;
                break;
        }
    }

    public static function put($name, $value)
    {
        if (self::$driver == null) {
            self::load();
        }
        return self::$driver->put($name, $value, -1);
    }

    public static function push($name, $value)
    {
        if (self::$driver == null) {
            self::load();
        }
        return self::$driver->push($name, $value);
    }

    public static function get($name, $fallback = null)
    {
        if (self::$driver == null) {
            self::load();
        }
        return self::$driver->get($name, $fallback);
    }

    public static function has($name)
    {
        if (self::$driver == null) {
            self::load();
        }
        return self::$driver->has($name);
    }

    public static function forget($name)
    {
        if (self::$driver == null) {
            self::load();
        }
        return self::$driver->forget($name);
    }

    public static function flush()
    {
        if (self::$driver == null) {
            self::load();
        }
        return self::$driver->flush();
    }

    public static function all()
    {
        if (self::$driver == null) {
            self::load();
        }
        return self::$driver->all();
    }

    public static function regenerate()
    {
        if (self::$driver == null) {
            self::load();
        }
        return self::$driver->regenerate();
    }
}
