<?php
defined('LIB_START') or exit('No direct script access allowed');

/**
* BattleNet regions.
*/
class BattleNetRegion
{
    const EU = 'eu';
    const US = 'us';
}
