<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'BattleNetClient.php';
require_once 'BattleNetRegion.php';
require_once 'BattleNetInterface.php';
require_once 'handler/WorldOfWarcraft.php';

class BattleNet extends ModuleProvider
{

    // The BattleNet client
    private static $client = null;

    // Cache engine
    protected static $engine = null;

    // BattleNet clients that are available.
    private static $clients = [
        BattleNetClient::WorldOfWarcraft
    ];

    // Contains the names of all the available resources.
    private static $resource = [];
    // Used as a cache for all loaded resources.
    private static $loadedResources = [];
    // All available resources. (This should be moved to the World of Warcraft client class)
    private static $resources = [
        'battlegroups/'         => 'battlegroups',
        'character/races'       => 'characterRaces',
        'character/classes'     => 'characterClasses',
        'character/achievements'=> 'characterAchievements',
        'guild/rewards'         => 'guildRewards',
        'guild/perks'           => 'guildPerks',
        'guild/achievements'    => 'guildAchievements',
        'item/classes'          => 'itemClasses',
        'talents'               => 'talents',
        'pet/types'             => 'petTypes'
    ];

    // Sets the default BattleNet region.
    private static $region = BattleNetRegion::EU;
    // All the available BattleNet regions.
    private static $regions = [
        BattleNetRegion::EU,
        BattleNetRegion::US
    ];

    /**
    * BattleNet constructor, will set our resources handlers.
    */
    public function __construct()
    {
        self::$engine = Cache::loadInstance('file', storage_path() . '/battlenet');
        // self::$path['main'] = storage_path() . '/battlenet/';
        // self::$path['data'] = self::$path['main'] . 'data/';
        // self::$path['res']  = self::$path['main'] . 'resources/';
    }

    /**
    * Sets our game client
    */
    public static function setClient($client)
    {
        self::$client = $client;
    }

    /**
    * Sets our region
    */
    public static function setRegion($region)
    {
        self::$region = $region;
    }

    /**
    * Initiate our BattleNet client, it is also possible to set our
    * client and region using this method.
    */
    public static function init($client = null, $region = null)
    {
        if (!is_null($client)) {
            self::setClient($client);
        }
        if (!is_null($region)) {
            self::setRegion($region);
        }

        // Check Client
        $clientEnabled = false;
        foreach (self::$clients as $client) {
            if (self::$client == $client) {
                $clientEnabled = true;
            }
        }
        if (!$clientEnabled) {
            throw new RuntimeException("Invalid client ID given!");
        }

        // Check Region
        $regionEnabled = false;
        foreach (self::$regions as $region) {
            if (self::$region == $region) {
                $regionEnabled = true;
            }
        }
        if (!$regionEnabled) {
            throw new RuntimeException("Invalid region ID given!");
        }

        // Load/Download resources
        foreach (self::$resources as $url => $name) {
            if (self::$engine->get('resources.' . $name) == null) {
                self::$engine->put('resources.' . $name, json_decode(self::getResource($url), false), Carbon::now()->addMonths(3));
            }
            self::$resource[] = $name;
        }

        // Sets our client to an instance of itself
        self::$client = new self::$client;
    }

    /**
    * Returns the game client.
    */
    public static function getClient()
    {
        if (!is_a(self::$client, 'BattleNetInterface')) {
            throw new RuntimeException("BattleNet has not yet been initialized.");
        }
        return self::$client;
    }

    /**
    * Returns the path array for all the resources.
    */
    public static function getPath()
    {
        return self::$path;
    }

    /**
    * Returns the region that was set to be used for the game client.
    */
    public static function getRegion()
    {
        return self::$region;
    }

    /**
    * Load a resource and return it as an array
    */
    public static function loadResource($name)
    {
        if (isset(self::$loadedResources[$name])) {
            return self::$loadedResources[$name];
        }

        if (!in_array($name, self::$resources)) {
            throw new RuntimeException("Invalid resource name given!");
        }

        $source = self::$engine->get('resources.' . $name, []);
        self::$loadedResources[$name] = $source;

        return $source;
    }

    /**
    * Downloads a resource off BattleNet's API.
    */
    private static function getResource($name)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://" . self::$region . ".battle.net/api/wow/data/{$name}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        if ($header['http_code'] != 200) {
            return false;
        }
        return $output;
    }
}
