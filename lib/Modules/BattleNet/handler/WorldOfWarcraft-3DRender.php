<?php
defined('LIB_START') or exit('No direct script access allowed');

class WorldOfWarcraft_3DRender
{
    private $instance;
    private $engine;

    public function __construct(WorldOfWarcraft $class, $engine)
    {
        $this->instance = $class;
        $this->engine   = $engine;
    }

    public function generate($char, $width, $height)
    {
        $data = $this->prepareCharacterRender(json_decode(json_encode($char), true));
        
        return '
		<object width="' . $width . 'px" height="' . $height . 'px" type="application/x-shockwave-flash" data="http://wow.zamimg.com/modelviewer/ZAMviewerfp11.swf" width="100%" height="100%">
			<param name="quality" value="high">
			<param name="wmode" value="direct"/>
			<param name="allowsscriptaccess" value="always">
			<param name="menu" value="false">
			<param name="flashvars" value="model=' . $data['racegender'] . '&amp;modelType=16&amp;contentPath=http://wow.zamimg.com/modelviewer/&amp;equipList=' . $data['appearance'] . '"/>
		</object>';
    }

    protected function prepareCharacterRender($data)
    {
        $render = [
            'head' => '',
            'shoulder' => '',
            'back' => '',
            'chest' => '',
            'shirt' => '',
            'tabard' => '',
            'wrist' => '',
            'hands' => '',
            'waist' => '',
            'legs' => '',
            'feet' => '',
            'mainHand' => '',
            'offHand' => ''
        ];

        if (isset($data['items']['head']) && $data['appearance']['showHelm']) {
            $render['head'] = $this->getDisplayId($data['items']['head']);
        } #голова
        if (isset($data['items']['shoulder'])) {
            $render['shoulder'] = $this->getDisplayId($data['items']['shoulder']);
        } #Плечи
        if (isset($data['items']['back']) && $data['appearance']['showCloak']) {
            $render['back'] = $this->getDisplayId($data['items']['back']);
        } #спина
        if (isset($data['items']['chest'])) {
            $render['chest'] = $this->getDisplayId($data['items']['chest']);
        } #грудь
        if (isset($data['items']['shirt'])) {
            $render['shirt'] = $this->getDisplayId($data['items']['shirt']);
        } #рубашка
        if (isset($data['items']['tabard'])) {
            $render['tabard'] = $this->getDisplayId($data['items']['tabard']);
        } #накидка
        if (isset($data['items']['wrist'])) {
            $render['wrist'] = $this->getDisplayId($data['items']['wrist']);
        } #брасы
        if (isset($data['items']['hands'])) {
            $render['hands'] = $this->getDisplayId($data['items']['hands']);
        } #руки
        if (isset($data['items']['waist'])) {
            $render['waist'] = $this->getDisplayId($data['items']['waist']);
        } #пояс
        if (isset($data['items']['legs'])) {
            $render['legs'] = $this->getDisplayId($data['items']['legs']);
        } #штаны
        if (isset($data['items']['feet'])) {
            $render['feet'] = $this->getDisplayId($data['items']['feet']);
        } #боты
        if (isset($data['items']['mainHand'])) {
            $render['mainHand'] = $this->getDisplayId($data['items']['mainHand']);
        } #пуха
        if (isset($data['items']['offHand'])) {
            $render['offHand'] = $this->getDisplayId($data['items']['offHand']);
        } #офф хэнд

        $renderString = '';
        foreach ($render as $renderItem) {
            if ($renderItem) {
                $renderString .= ',' . $renderItem;
            }
        }

        $renderData['thumbnail'] = $data['thumbnail'];
        $renderData['face'] = $data['appearance']['faceVariation'];
        $renderData['skin'] = $data['appearance']['skinColor'];
        $renderData['hairv'] = $data['appearance']['hairVariation'];
        $renderData['hairc'] = $data['appearance']['hairColor'];
        $renderData['feature'] = $data['appearance']['featureVariation'];
        $renderData['appearance'] = substr($renderString, 1, strlen($renderString) - 1) .
        '&sk='.$renderData['skin'].
        '&ha='.$renderData['hairv'].
        '&hc='.$renderData['hairc'].
        '&fa='.$renderData['face'].
        '&fh='.$renderData['feature'].
        '&fc='.$renderData['feature'].
        '&mode=3';
        $renderData['racegender'] = mb_strtolower(str_replace(' ', '', $data['race']) . $data['gender']);
        $renderData['racegender'] = str_replace('undead', 'scourge', $renderData['racegender']);
        return $renderData;
    }

    protected function getDisplayId($item)
    {
        if ($item == null) {
            return;
        }
        if (isset($item['tooltipParams']) && isset($item['tooltipParams']['transmogItem'])) {
            $id = $item['tooltipParams']['transmogItem'];
        } else {
            $id = $item['id'];
        }

        $cache = $this->engine->get('items.' . $id);
        if ($cache != null) {
            return $cache;
        }

        $url = 'http://www.wowhead.com/item=' . $id . '&xml';
        $ch = curl_init($url);
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.66 Safari/537.36');
        $response = curl_exec($ch);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $response, $vals, $index);
        xml_parser_free($p);
        $position = strstr(strstr($vals[$index['JSON'][0]]['value'], 'slotbak":'), ',', true);
        $position = substr(strstr($position, ':'), 1);
        $position = trim($position);
        $data = $position . ',' . $vals[$index['ICON'][0]]['attributes']['DISPLAYID'];
        
        $this->engine->put('items.' . $id, $data, Carbon::now()->addMonths(3));
        
        return $data;
    }
}
