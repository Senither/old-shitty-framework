<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'WorldOfWarcraft-3DRender.php';

class WorldOfWarcraft extends BattleNet implements BattleNetInterface
{
    // This is our handler version number.
    private $version = '0.1';

    private $region;
    private $guildPath;
    private $realmPath;
    private $characterPath;
    
    public $WoWHeadItemPath;

    private $render = null;

    // Default cache character data, makes re-calling functions easier.
    private $character = ['character' => null, 'realm' => null];
    // Character cache data to limet the out-going calls.
    private $characterCache = [];

    public function __construct()
    {
        $this->region = BattleNet::getRegion();
    }
    
    public function getClientID()
    {
        return explode(':', __METHOD__)[0] . ' ' . $this->version;
    }

    /**
    * Sets our character and realm so we're ready to make character request calls.
    */
    public function setCharacter($character, $realm)
    {
        $this->character['character'] = $character;
        $this->character['realm'] = $realm;
    }

    /**
    * Gets our character request call data.
    */
    public function getCharacter($character = null, $realm = null)
    {
        // Checks if we included a new set of character info, if we did we just use that instead.
        if ($character != null && $realm != null) {
            $this->setCharacter($character, $realm);
        }

        $name = $this->character['character'] . '-' . $this->character['realm'] . '-' . $this->region;

        // Checks if we have a valid cache of the player on our server, will incress the page load time.
        if (parent::$engine->has('characters.' . $name)) {

            // dd(parent::$engine->get('characters.' . $name));
            $this->characterCache[$name] = parent::$engine->get('characters.' . $name);
            return $this->characterCache[$name];
        }

        // Sends our character request call.
        $response = $this->sendRequest('character', [$realm, $this->character['character']], ['feed', 'guild', 'professions', 'progression', 'pvp', 'stats', 'titles', 'appearance', 'items'], true);
        
        // Checks to see if we got a valid character response.
        if (!isset($response['status'])) {
            // Replaces ID values with readable information from our resource cache.
            $response = $this->replaceValue('class', $response, 'characterClasses');
            $response = $this->replaceValue('race', $response, 'characterRaces');
            $response['gender'] = ($response['gender'] == 0) ? 'Male' : 'Female';
        }

        // Sets our character cache.
        $this->characterCache[$name] = $response;

        // Save cache and return
        parent::$engine->put('characters.' . $name, $response, Carbon::now()->addDays(3)->addHours(12));
        return $response;
    }

    /**
    * Gets the characters profile image.
    */
    public function getCharacterImage()
    {
        $char = $this->getCharacter($this->character['character'], $this->character['realm']);
        if (isset($char->thumbnail)) {
            return "http://{$this->region}.battle.net/static-render/{$this->region}/" . $char->thumbnail;
        }
    }

    /**
    * Generates a link to the characters wow armory page.
    */
    public function getCharacterLink($advanced = true)
    {
        $char = $this->getCharacter($this->character['character'], $this->character['realm']);
        return "http://{$this->region}.battle.net/wow/en/character/" . utf8_decode($char->realm) . "/" . utf8_decode($char->name) . "/" . (($advanced) ? 'advanced' : '');
    }

    /**
    * Generates a 3D render of the character with the width or height
    */
    public function getCharacterRender($width = 300, $height = 500)
    {
        if ($this->render == null) {
            $this->render = new WorldOfWarcraft_3DRender($this, parent::$engine);
        }

        $character = $this->getCharacter($this->character['character'], $this->character['realm']);

        return $this->render->generate($character, $width, $height);
    }

    /**
    * Gets our guild request call data.
    */
    public function getGuild($guild, $realm)
    {
        // Checks if we have a valid cache of the guild on our server, will incress the page load time.
        $hash = utf8_decode($guild . '-' . $realm . '-' . $this->region);
        if (($cache = Cache::get($this->guildPath . $hash, 259200)) != null) {
            return json_decode($cache, true);
        }

        // Sends our guild request call.
        $response = $this->sendRequest('guild', [$realm, $guild], ['members', 'news', 'challenge'], true);

        // Checks to see if we got a valid guild response.
        if (!isset($response['status'])) {
            $members = $response['members'];

            // Loops through all the members of the guild and replaces the ID values
            // with readable information from our resource cache.
            foreach ($members as $index => $member) {
                $member = $member['character'];
                $member = $this->replaceValue('class', $member, 'characterClasses');
                $member = $this->replaceValue('race', $member, 'characterRaces');
                $member['gender'] = ($member['gender'] == 0) ? 'Male' : 'Female';
                $response['members'][$index]['character'] = $member;
            }
        }

        // Save cache and return
        Cache::set($this->guildPath . $hash, json_encode($response));
        return $response;
    }

    public function getGuildWorldRank($guild, $realm)
    {
        $hash = utf8_decode($guild . '-' . $realm . '-' . $this->region . '-rank');
        if (($cache = parent::$engine->get('guilds.' . $hash)) != null) {
            return $cache;
        }
        $realm = urlencode($realm);
        $guild = urlencode($guild);

        $json = json_decode(file_get_contents("http://www.wowprogress.com/guild/eu/{$realm}/{$guild}/json_rank"), true);

        parent::$engine->put('guilds.' . $hash, $json, Carbon::now()->addHours(6));
        return $json;
    }

    /**
    * Get the status of a realm, you define a realm you want the info for.
    */
    public function getRealmStatus($realm = null)
    {
        $response = null;

        // Checks if we have a valid cache of the realms on our server.
        $hash = utf8_decode('realmStatus' . $this->region);
        if (($cache = parent::$engine->get('realms.' . $hash)) != null) {
            $response = $cache;
        } else {
            $response = $this->sendRequest('realm', ['status'], null, true);
            parent::$engine->put('realms.' . $hash, $response, Carbon::now()->addMinutes(30));
        }

        // Checks to see if we defined a realm to get the info off,
        // otherwise we'll just return all the realms and their status.
        if ($realm != null) {
            foreach ($response as $arr) {
                foreach ($arr as $obj) {
                    if ($obj['name'] == $realm) {
                        return $obj;
                    }
                }
            }
            return null;
        }
        
        return $response;
    }

    /**
    * Used to replace values from a multidimensional array based off a resource and key.
    *
    * @param name The index to replace
    * @param obj The array to replace the index from
    * @param res The resource name
    */
    private function replaceValue($name, $obj, $res)
    {
        $value = $obj[$name];

        // Loads the defined resource and loops throught the content.
        foreach (BattleNet::loadResource($res) as $k => $a) {
            foreach ($a as $i => $v) {
                if ($v->id == $value) {
                    $obj[$name] = $v->name;
                    return $obj;
                }
            }
        }

        return $obj;
    }

    /**
    * Used to send a data request call to Blizzards WoW API.
    *
    * @param func The function to use when calling the API
    * @param parms The function parameters
    * @param selector Function selectors, used to gather extra information from the request
    * @param trim Determs if the url should be trimed for the character '/' or not
    */
    private function sendRequest($func, $param, $selector = null, $trim = false)
    {
        // Generates the request call URL.
        $url = "http://{$this->region}.battle.net/api/wow/{$func}/";
        foreach ($param as $key) {
            $url .= str_replace(' ', '%20', $key) . '/';
        }

        // Removes the '/' character at the end of the url if trim is set to true.
        $url = ($trim) ? trim($url, '/') : $url;

        // Generates the selectors if there is any.
        if ($selector != null && is_array($selector)) {
            $selectors = '';
            foreach ($selector as $key) {
                $selectors .= $key . ',';
            }
            $url .= '?fields=' . trim($selectors, ',');
        }

        // initialize the curl object.
        $ch = curl_init();
        // Sets our curl options.
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Sends your request to the API.
        $output = curl_exec($ch);
        // Get the header responde.
        $header = curl_getinfo($ch);
        // Close your connection.
        curl_close($ch);

        // Check if our responde code is 200,
        // We're not using this at the moment.
        if ($header['http_code'] != 200) {
            // return false;
        }
        return json_decode($output, true);
    }
}
