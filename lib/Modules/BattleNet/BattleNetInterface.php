<?php
defined('LIB_START') or exit('No direct script access allowed');

/**
* BattleNet client interface.
*/
interface BattleNetInterface
{
    public function getClientID();
}
