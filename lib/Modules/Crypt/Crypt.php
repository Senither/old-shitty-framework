<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'CryptType.php';
require_once 'CryptException.php';

class Crypt extends ModuleProvider
{

    /**
     * A list of characters that are used to generated the random salts for passwords.
     * @var string
     */
    private static $characters = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM+';

    /**
     * An array of all the available types of Crypt Types, every type also has their own callable method.
     * @var Array
     */
    private static $types = [
        CryptType::MD5,
        CryptType::GOST,
        CryptType::SNEFRU,
        CryptType::PASSWORD,
        CryptType::WHIRLPOOL,
        CryptType::SHA1, CryptType::SHA256, CryptType::SHA512,
        CryptType::TIGER128, CryptType::TIGER160, CryptType::TIGER192,
        CryptType::RIPED128, CryptType::RIPED160, CryptType::RIPED256, CryptType::RIPED320,
        CryptType::GAVAL128, CryptType::GAVAL160, CryptType::GAVAL192, CryptType::GAVAL256
    ];

    /**
     * Empty constructor because it's required by PHP when we're calling static methods.
     */
    public function __construct()
    {
    }

    /**
     * Validates the encryption type given to see if it's supported.
     *
     * @param  String  $type The CryptType to validate.
     * @return boolean       Returns true if the type was found.
     */
    private static function isValidCryptType($type)
    {
        return in_array($type, self::$types);
    }

    /**
     * Encrypts the hash given with the type given.
     *
     * @param  CryptType  $type      The type to encrypt the string with.
     * @param  String     $hash      The hash to encrypt.
     * @param  boolean    $lowerCase Makes the hash lowercase.
     * @param  String     $salt      Gives a pre-defined salt to a hash.
     * @param  Integer    $cost      Adds a cost, will by default be 10.
     * @return String                Returns the encrypted hash.
     */
    public static function hash($type, $hash, $lowerCase = false, $salt = null, $cost = 10)
    {
        if (!self::isValidCryptType($type)) {
            throw new CryptException("Invalid or Unsupported cryped type given!");
            return null;
        }

        $hash = ($lowerCase) ? mb_strtolower($hash) : $hash;
        $hash = ($salt == null) ? $hash : $salt . $hash;

        switch ($type) {
            case 'password':
                $salt = str_replace("+", ".", self::generateSalt());
                $param = '$' . implode('$', array("2y", str_pad($cost, 2, "0", STR_PAD_LEFT), $salt));
                return crypt($hash, $param);

            default:
                return hash($type, $hash);
        }
    }

    /**
     * Make a encrypted hash of a password using BCrypt.
     *
     * @param  String  $password  The password to encrypt.
     * @param  Integer $cost      The cost of the password.
     * @param  Boolean $lowerCase Makes the password lowercase.
     * @return String             The newly encrypted password
     */
    public static function make($password, $cost = 10, $lowerCase = false)
    {
        return self::hash(CryptType::PASSWORD, $password, $lowerCase, null, $cost);
    }

    /**
     * Match an encrypted password with the given hash to see if they match
     * @param  String  $password  The password to match with the hash.
     * @param  String  $hash      The encrypted hash to match with the password.
     * @param  Boolean $lowerCase Makes the password lowercase.
     * @return Boolean            Returns true if they match, false otherwise.
     */
    public static function match($password, $hash, $lowerCase = false)
    {
        return crypt(($lowerCase) ? mb_strtolower($password) : $password, $hash) == $hash;
    }

    /**
     * Generates a random slat with the given length.
     *
     * @param  Integer $length The length of the salt.
     * @return String          The newly generated salt.
     */
    private static function generateSalt($length = 64, $salt = '')
    {
        $l = mb_strlen(self::$characters);
        for ($i = 0; $i < $length; $i++) {
            $salt .= self::$characters[rand(0, ($l - 1))];
        }
        return $salt;
    }

    /////////////////////////////////////////////////////////////////////////////////
    // --------------------------------------------------------------------------- //
    // ----------------------- Encryption Callable Methods ----------------------- //
    // --------------------------------------------------------------------------- //
    /////////////////////////////////////////////////////////////////////////////////

    public static function md5($hash, $salt = null)
    {
        return self::hash(CryptType::MD5, $hash, false, $salt);
    }

    public static function gost($hash, $salt = null)
    {
        return self::hash(CryptType::GOST, $hash, false, $salt);
    }

    public static function snefru($hash, $salt = null)
    {
        return self::hash(CryptType::SNEFRU, $hash, false, $salt);
    }

    public static function whirlpool($hash, $salt = null)
    {
        return self::hash(CryptType::WHIRLPOOL, $hash, false, $salt);
    }

    public static function sha1($hash, $salt = null)
    {
        return self::hash(CryptType::SHA1, $hash, false, $salt);
    }

    public static function sha256($hash, $salt = null)
    {
        return self::hash(CryptType::SHA256, $hash, false, $salt);
    }

    public static function sha512($hash, $salt = null)
    {
        return self::hash(CryptType::SHA512, $hash, false, $salt);
    }

    public static function tiger128($hash, $salt = null)
    {
        return self::hash(CryptType::TIGER128, $hash, false, $salt);
    }

    public static function tiger160($hash, $salt = null)
    {
        return self::hash(CryptType::TIGER160, $hash, false, $salt);
    }

    public static function tiger192($hash, $salt = null)
    {
        return self::hash(CryptType::TIGER192, $hash, false, $salt);
    }

    public static function riped128($hash, $salt = null)
    {
        return self::hash(CryptType::RIPED128, $hash, false, $salt);
    }

    public static function riped160($hash, $salt = null)
    {
        return self::hash(CryptType::RIPED160, $hash, false, $salt);
    }

    public static function riped256($hash, $salt = null)
    {
        return self::hash(CryptType::RIPED256, $hash, false, $salt);
    }

    public static function riped320($hash, $salt = null)
    {
        return self::hash(CryptType::RIPED320, $hash, false, $salt);
    }

    public static function gaval128($hash, $salt = null)
    {
        return self::hash(CryptType::GAVAL128, $hash, false, $salt);
    }

    public static function gaval160($hash, $salt = null)
    {
        return self::hash(CryptType::GAVAL160, $hash, false, $salt);
    }

    public static function gaval192($hash, $salt = null)
    {
        return self::hash(CryptType::GAVAL192, $hash, false, $salt);
    }

    public static function gaval256($hash, $salt = null)
    {
        return self::hash(CryptType::GAVAL256, $hash, false, $salt);
    }
}
