<?php
defined('LIB_START') or exit('No direct script access allowed');

class CryptType
{
    const MD5 = 'md5';
    const SHA1 = 'sha1';
    const SHA256 = 'sha256';
    const SHA512 = 'sha512';
    const RIPED128 = 'ripemd128';
    const RIPED160 = 'ripemd160';
    const RIPED256 = 'ripemd256';
    const RIPED320 = 'ripemd320';
    const WHIRLPOOL = 'whirlpool';
    const TIGER128 = 'tiger128,4';
    const TIGER160 = 'tiger160,4';
    const TIGER192 = 'tiger192,4';
    const SNEFRU = 'snefru';
    const GOST = 'gost';
    const GAVAL128 = 'haval128,5';
    const GAVAL160 = 'haval160,5';
    const GAVAL192 = 'haval192,5';
    const GAVAL256 = 'haval256,5';
    const PASSWORD = 'password';
}
