<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'GeoObject.php';

class Geo extends ModuleProvider
{
    protected static $api = "http://www.telize.com/geoip/%s";

    protected static $engine = null;

    public function __construct()
    {
        self::$engine = Cache::loadInstance('file', storage_path() . '/geo');
    }

    public static function request($ip)
    {
        if (!filter_var($ip, FILTER_VALIDATE_IP)) {
            return [];
        }

        $hash = str_replace('.', '-', $ip);

        if (self::$engine->has($hash)) {
            return new GeoObject(self::$engine->get($hash));
        }

        $url = sprintf(self::$api, $ip);
        $data = self::sendRequest($url);
        $data = json_decode($data, true);

        if (isset($data['code'])) {
            self::$engine->put($hash, [], Carbon::now()->addDays(7));
            return new GeoObject([]);
        }

        self::$engine->put($hash, $data, Carbon::now()->addDays(7));
        return new GeoObject($data);
    }

    protected static function sendRequest($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);

        return curl_exec($curl);
    }

    public static function getRawIp()
    {
        if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            return $_SERVER['HTTP_CF_CONNECTING_IP'];
        } elseif (isset($_SERVER['HTTP_X_REAL_IP'])) {
            return $_SERVER['HTTP_X_REAL_IP'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            return $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            return $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return 'UNKNOWN';
    }
}
