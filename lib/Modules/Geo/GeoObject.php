<?php
defined('LIB_START') or exit('No direct script access allowed');

class GeoObject
{
    private $properties = [];

    public function __construct($properties)
    {
        $this->properties = $properties;
    }

    public function __get($name)
    {
        if (isset($this->properties[$name])) {
            return $this->properties[$name];
        }
        throw new ErrorException('Undefined property: GeoObject::$' . $name);
    }

    public function __set($name, $value)
    {
        if (isset($this->properties[$name])) {
            return $this->properties[$name] = $value;
        }
        throw new ErrorException('Undefined property: GeoObject::$' . $name);
    }
}
