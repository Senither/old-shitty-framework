<?php
defined('LIB_START') or exit('No direct script access allowed');

class URL extends ModuleProvider
{
    private static $host;
    private static $secure;

    private static $prefix;
    private static $suffix;

    public function __construct()
    {
        self::$host = $_SERVER['HTTP_HOST'];

        self::$secure = Config::get('url.secure', false);
        self::$prefix = Config::get('url.environments.' . ENVIRONMENT . '.prefix', '');
        self::$suffix = Config::get('url.environments.' . ENVIRONMENT . '.suffix', '');
    }

    public static function current($params = [], $secure = false)
    {
        return self::build($secure, $_SERVER['PHP_SELF'], self::$prefix, null) . self::urlencode($params);
    }

    public static function secure($link, $params = [])
    {
        return self::to($link, $params, true);
    }

    public static function to($link, $params = [], $secure = false)
    {
        return self::build($secure, $link, self::$prefix, self::$suffix) . self::urlencode($params);
    }

    public static function secure_asset($file, $title, $attributes = array())
    {
        return self::asset($file, $title, $attributes, true);
    }

    public static function asset($file, $attributes = array(), $secure = false)
    {
        $str = null;

        $end = explode('.', $file);
        $end = end($end);

        switch ($end) {
            case 'js':
                $str = '<script type="text/javascript"%s src="%s"></script>';
                break;

            case 'css':
                $str = '<link rel="stylesheet" type="text/css"%s href="%s" />';
                break;

            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
            case 'bmp':
            case 'tiff':
                $str = '<img%s src="%s" />';
                break;

            default:
                throw new RuntimeException("Invalid or unsupported value parsed to the asset method");
        }

        $attr = '';
        foreach ($attributes as $key => $value) {
            $attr .= " {$key}=\"{$value}\"";
        }

        return sprintf($str . "\r\n", $attr, self::build($secure, $file, self::$prefix, self::$suffix));
    }

    private static function build($secure, $link, $prefix, $suffix)
    {
        if (mb_strlen($suffix) == 0) {
            $suffix = '/';
        } elseif (!String::endsWith($suffix, '/')) {
            $suffix .= '/';
        }

        if (String::startWith($link, '/')) {
            $link = mb_substr($link, 1);
        }

        return self::getScheme($secure) . $prefix . self::$host . $suffix . $link;
    }

    private static function getScheme($secure)
    {
        if ($secure) {
            return 'https://';
        }
        return 'http' . ((self::$secure) ? 's' : null) . '://';
    }

    private static function urlencode($arguments)
    {
        if (empty($arguments)) {
            return null;
        }
        foreach ($arguments as $name => $value) {
            $arguments[$name] = $name . '=' . urlencode($value);
        }
        return '?' . implode('&', $arguments);
    }
}
