<?php
defined('LIB_START') or exit('No direct script access allowed');

class String extends ModuleProvider implements StringParserProvider
{
    private static $characters = '0123456789';

    public function __loadModule()
    {
        foreach (range('A', 'z') as $char) {
            self::$characters .= $char;
        }

        require_once 'functions.php';
    }

    /**
     * Checks to see if a string is numeric.
     *
     * @param  String  $string The string to check.
     * @return Boolean         True if is numeric, false otherwise.
     */
    public static function isNumeric($string)
    {
        return is_numeric($string);
    }

    /**
     * Checks to see if a string is alphabetic.
     *
     * @param  String  $string The string to check.
     * @return Boolean         True if is alphabetic, false otherwise.
     */
    public static function isAlphabetic($string)
    {
        return ctype_alpha($string);
    }

    /**
     * Checks to see if a string is alphanumeric.
     *
     * @param  String  $string The string to check.
     * @return Boolean         True if is alphanumeric, false otherwise.
     */
    public static function isAlphanumeric($string)
    {
        return ctype_alnum($string);
    }

    /**
     * Checks to see if a string is an email.
     *
     * @param  String  $string The string to check.
     * @return Boolean         True if is an email, false otherwise.
     */
    public static function isEmail($string)
    {
        return filter_var($string, FILTER_VALIDATE_EMAIL) === $string;
    }

    /**
     * Checks to see if a string is a URL.
     *
     * @param  String  $string The string to check.
     * @return Boolean         True if is a URL, false otherwise.
     */
    public static function isURL($string)
    {
        return filter_var($string, FILTER_VALIDATE_URL) === $string;
    }

    /**
     * Checks to see if the string starts with the given character(s)
     *
     * @param  String  $character The string to check agaist
     * @param  integer $push      Pushes the string {$push} amount of characters
     * @param  boolean $strict    Determes if the check should be strict or not
     * @return boolean            True if the string ends with the characters given, otherwise false
     */
    public static function startWith($string, $character, $push = 0, $strict = false)
    {
        $line = mb_substr($string, $push, mb_strlen($character));
        return ($strict) ? $line === $character : mb_strtolower($line) == mb_strtolower($character);
    }

    /**
     * Checks to see if the string ends with the given character(s)
     *
     * @param  String  $character The string to check agaist
     * @param  integer $push      Pushes the string {$push} amount of characters
     * @param  boolean $strict    Determes if the check should be strict or not
     * @return boolean            True if the string ends with the characters given, otherwise false
     */
    public static function endsWith($string, $character, $push = 0, $strict = false)
    {
        $line = mb_substr($string, (mb_strlen($string) - $push) - mb_strlen($character), mb_strlen($character));
        return ($strict) ? $line === $character : mb_strtolower($line) == mb_strtolower($character);
    }

    /**
     * Get the character at a given index of a string, if the index supasses the
     * strings lenght it will return an empty string.
     *
     * @param  integer $index The index to look at
     * @return String         The character at the given index
     */
    public static function charAt($string, $index)
    {
        return mb_substr($string, $index, 1);
    }

    /**
     * Convert the given string to camelCase.
     * Example: foo_bar -> fooBar
     *
     * @return String The new string value
     */
    public static function camel_case($string)
    {
        $arr = preg_split("/(_| )/", $string);
        $str = mb_strtolower($arr[0]);

        for ($i = 1; $i < count($arr); $i++) {
            $str .= mb_strtoupper(mb_substr($arr[$i], 0, 1)) .
                    mb_strtolower(mb_substr($arr[$i], 1, mb_strlen($arr[$i])));
        }

        return $str;
    }

    /**
     * Convert the given string to snake_case.
     * Example: fooBar -> foo_bar
     *
     * @return String The new string value
     */
    public static function snake_case($string)
    {
        $arr = [];
        $x = 0;

        for ($i = 0; $i < mb_strlen($string); $i++) {
            if (ctype_upper($string[$i])) {
                if ($i == 0) {
                    continue;
                }

                $arr[] = mb_substr($string, $x, $i - $x);
                $x = $i;
            }
        }

        $arr[] = mb_substr($string, $x, mb_strlen($string));
        return $string = mb_strtolower(implode('_', $arr));
    }

    /**
     * Convert the given string to snake_case.
     * Example: foo_bar -> FooBar
     *
     * @return String The new string value
     */
    public static function studly_case($string)
    {
        $arr = preg_split("/(_| )/", $string);
        $str = '';
        for ($i = 0; $i < count($arr); $i++) {
            $str .= mb_strtoupper(mb_substr($arr[$i], 0, 1)) .
                    mb_strtolower(mb_substr($arr[$i], 1, mb_strlen($arr[$i])));
        }

        return $str;
    }

    /**
     * Limit the number of characters in a string.
     *
     * @param  integer $limit The amount of characters to limit the string by.
     * @param  String  $end   What to display at the end of the string.
     * @return String         The new and shorter string.
     */
    public static function limit($string, $limit = 100, $end = '...')
    {
        return mb_substr($string, 0, $limit) . $end;
    }

    /**
     * Checks the string agaist the text given to see if the text is in the string somewhere.
     *
     * @param  String $text The text to check against
     * @return Boolean      True if the text is found in the string, false otherwise
     */
    public static function cotains($string, $text)
    {
        return preg_match(sprintf("/%s/i", preg_quote($text, '/')), $string) != 0;
    }

    /**
     * Will make sure the string ends with the given character or list of characters,
     * if the string already ends with the given string it won't do anything.
     *
     * @param  String $character The list of characters to end the string with.
     * @return String            The new string if it has been changed.
     */
    public static function finish($string, $character)
    {
        if (!self::endsWith($string, $character)) {
            $string .= $character;
        }
        return $string;
    }

    /**
     * Generates a random string of characters of the length given.
     *
     * @param  Ínteger  $length    The length of the random string
     * @param  boolean $useCustom  Forces the method to use the custom generator, othwise it will try
     *                             to use the openssl randoms function if it is installed on the server.
     * @return String              The randomly generated string.
     */
    public static function random($length = 16, $useCustom = false)
    {
        if (function_exists('openssl_random_pseudo_bytes') && $useCustom === false) {
            return mb_substr(preg_replace("/[\/=+]/", "", base64_encode(openssl_random_pseudo_bytes($length))), 0, $length);
        }
        $result = '';
        for ($i = 0; $i < $length; $i++) {
            $result .= self::$characters[rand(0, (mb_strlen(self::$characters) - 1))];
        }
        return $result;
    }
}
