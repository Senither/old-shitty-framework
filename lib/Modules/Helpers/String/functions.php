<?php
defined('LIB_START') or exit('No direct script access allowed');

if (!function_exists('starts_with')) {

    /**
     * Checks to see if the string starts with the given character(s)
     *
     * @param  String  $character The string to check agaist
     * @param  integer $push      Pushes the string {$push} amount of characters
     * @param  boolean $strict    Determes if the check should be strict or not
     * @return boolean            True if the string ends with the characters given, otherwise false
     */
    function starts_with($string, $character, $push = 0, $strict = false)
    {
        return String::startWith($string, $character, $push, $strict);
    }
}

if (!function_exists('ends_with')) {

    /**
     * Checks to see if the string ends with the given character(s)
     *
     * @param  String  $character The string to check agaist
     * @param  integer $push      Pushes the string {$push} amount of characters
     * @param  boolean $strict    Determes if the check should be strict or not
     * @return boolean            True if the string ends with the characters given, otherwise false
     */
    function ends_with($string, $character, $push = 0, $strict = false)
    {
        return String::endsWith($string, $character, $push, $strict);
    }
}

if (!function_exists('str_charAt')) {

    /**
     * Get the character at a given index of a string, if the index supasses the
     * strings lenght it will return an empty string.
     *
     * @param  integer $index The index to look at
     * @return String         The character at the given index
     */
    function str_charAt($string, $index)
    {
        return String::charAt($string, $index);
    }
}

if (!function_exists('camel_case')) {

    /**
     * Convert the given string to camelCase.
     * Example: foo_bar -> fooBar
     *
     * @return String The new string value
     */
    function camel_case($string)
    {
        return String::camel_case($string);
    }
}

if (!function_exists('snake_case')) {

    /**
     * Convert the given string to snake_case.
     * Example: fooBar -> foo_bar
     *
     * @return String The new string value
     */
    function snake_case($string)
    {
        return String::snake_case($string);
    }
}

if (!function_exists('studly_case')) {

    /**
     * Convert the given string to snake_case.
     * Example: foo_bar -> FooBar
     *
     * @return String The new string value
     */
    function studly_case($string)
    {
        return String::studly_case($string);
    }
}

if (!function_exists('str_limit')) {

    /**
     * Limit the number of characters in a string.
     *
     * @param  integer $limit The amount of characters to limit the string by.
     * @param  String  $end   What to display at the end of the string.
     * @return String         The new and shorter string.
     */
    function str_limit($string, $limit = 100, $end = '...')
    {
        return String::limit($string, $limit, $end);
    }
}

if (!function_exists('str_contains')) {

    /**
     * Checks the string agaist the text given to see if the text is in the string somewhere.
     *
     * @param  String $text The text to check against
     * @return Boolean      True if the text is found in the string, false otherwise
     */
    function str_contains($string, $text)
    {
        return String::cotains($string, $text);
    }
}

if (!function_exists('str_finish')) {

    /**
     * Checks the string agaist the text given to see if the text is in the string somewhere.
     *
     * @param  String $text The text to check against
     * @return Boolean      True if the text is found in the string, false otherwise
     */
    function str_finish($string, $character)
    {
        return String::finish($string, $character);
    }
}

if (!function_exists('str_random')) {

    /**
     * Generates a random string of characters of the length given.
     *
     * @param  Ínteger  $length    The length of the random string
     * @param  boolean $useCustom  Forces the method to use the custom generator, othwise it will try
     *                             to use the openssl randoms function if it is installed on the server.
     * @return String              The randomly generated string.
     */
    function str_random($length, $useCustom = false)
    {
        return String::random($length);
    }
}
