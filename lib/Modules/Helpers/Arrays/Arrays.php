<?php
defined('LIB_START') or exit('No direct script access allowed');

class Arrays extends ModuleProvider implements StringParserProvider
{
    public function __loadModule()
    {
        require_once 'functions.php';
    }

    /**
     * Adds a given key / value pair to the array if the given key doesn't already exist in the array.
     *
     * @param String $key   The new array key.
     * @param Object $value The value of the key.
     * @return Array        The new array
     */
    public static function add($array, $key, $value)
    {
        if (!isset($array[$key])) {
            $array[$key] = $value;
        }
        return $array;
    }

    /**
     * The divide function returns two arrays, one containing the keys, and the other containing
     * the values of the original array.
     *
     * @return Array        The new version of the array.
     */
    public static function divide($array)
    {
        $keys = array();
        $values = array();

        foreach ($array as $k => $v) {
            $keys[] = $k;
            $values[] = $v;
        }

        return [$keys, $values];
    }

    /**
     * The dot function flattens a multi-dimensional array into a single level array that uses "dot"
     * notation to indicate depth.
     *
     * @return Array        The new version of the array.
     */
    public static function dot($array)
    {
        $result = array();
        foreach ($array as $first => $value) {
            foreach (self::dot_recursive(mb_strtolower($first), $value, array()) as $path => $arr) {
                $result[$path] = $arr;
            }
        }
        return $result;
    }

    private static function dot_recursive($path, $content, $writeable)
    {
        if (is_array($content)) {
            foreach ($content as $first => $value) {
                if (is_array($value)) {
                    return self::dot_recursive($path . '.' . $first, $value, $writeable);
                }
                $writeable[$path . '.' . $first] = $value;
            }
        } else {
            $writeable[$path] = $content;
        }
        return $writeable;
    }

    /**
     * The except method removes the given key / value pairs from the array.
     *
     * @param  Array $array An array of values to remove from the object.
     * @return Array        The new version of the array.
     */
    public static function except($array, $keySet)
    {
        if (is_array($keySet)) {
            foreach ($keySet as $name) {
                if (!is_array($name) && isset($array[$name])) {
                    unset($array[$name]);
                }
            }
        } elseif (isset($array[$keySet])) {
            unset($array[$keySet]);
        }
        return $array;
    }

    /**
     * The fetch method returns a flattened array containing the selected nested element.
     * Notice: Upper and lower case matters here..
     *
     * @param  String $name     The path of the array to get as an array dot notation.
     * @return Array            The new version of the array.
     */
    public static function fetch($array, $name)
    {
        $parsed = explode('.', $name);

        while ($parsed) {
            $next = array_shift($parsed);

            if (isset($array[$next])) {
                $array = $array[$next];
            } else {
                return null;
            }
        }

        return $array;
    }

    /**
     * The first method returns the first element of an array passing a given truth test.
     *
     * @param  Function $callback   The function callback for the truth test, the function
     *                              takes in two arguments, the key and the value.
     * @return Object               The first object that parses the truth test.
     */
    public static function first($array, $callback)
    {
        foreach ($array as $key => $value) {
            if (call_user_func_array($callback, [$key, $value])) {
                return $value;
            }
        }
    }

    /**
     * The last method returns the last element of an array passing a given truth test.
     *
     * @param  Function $callback   The function callback for the truth test, the function
     *                              takes in two arguments, the key and the value.
     * @return Object               The last object that parses the truth test.
     */
    public static function last($array, $callback)
    {
        foreach (array_reverse($array) as $key => $value) {
            if (call_user_func_array($callback, [$key, $value])) {
                return $value;
            }
        }
    }

    /**
     * The flatten method will flatten a multi-dimensional array into a single level.
     *
     * @return Array The new flattened array.
     */
    public static function flatten($array, $flatten = [])
    {
        foreach ($array as $item) {
            if (is_array($item)) {
                $flatten = self::flatten($item, $flatten);
                continue;
            }

            $flatten[] = $item;
        }

        return $flatten;
    }

    /**
     * Returns a random index of the given array.
     *
     * @param  Array $array The array to pick from
     * @return Object       The value of the randomly picked index of the array.
     */
    public static function random($array)
    {
        return $array[array_rand($array, 1)];
    }
}
