<?php
defined('LIB_START') or exit('No direct script access allowed');

if (!function_exists('array_add')) {

    /**
     * Adds a given key / value pair to the array if the given key doesn't already exist in the array.
     *
     * @param String $key   The new array key.
     * @param Object $value The value of the key.
     * @return Array        The new array
     */
    function array_add($array, $key, $value)
    {
        return Arrays::add($array, $key, $value);
    }
}

if (!function_exists('array_divide')) {

     /**
     * The divide function returns two arrays, one containing the keys, and the other containing
     * the values of the original array.
     *
     * @return Array        The new version of the array.
     */
    function divide($array)
    {
        return Arrays::divide($array);
    }
}

if (!function_exists('array_dot')) {

     /**
     * The dot function flattens a multi-dimensional array into a single level array that uses "dot"
     * notation to indicate depth.
     *
     * @return Array        The new version of the array.
     */
     function array_dot($array)
     {
         return Arrays::dot($array);
     }
}

if (!function_exists('array_except')) {

     /**
     * The except method removes the given key / value pairs from the array.
     *
     * @param  Array $array An array of values to remove from the object.
     * @return Array        The new version of the array.
     */
     function array_except($array, $keySet)
     {
         return Arrays::except($array, $keySet);
     }
}

if (!function_exists('array_fetch')) {

     /**
     * The fetch method returns a flattened array containing the selected nested element.
     * Notice: Upper and lower case matters here..
     *
     * @param  String $name     The path of the array to get as an array dot notation.
     * @return Array            The new version of the array.
     */
     function array_fetch($array, $name)
     {
         return Arrays::fetch($array, $name);
     }
}

if (!function_exists('array_first')) {

     /**
     * The first method returns the first element of an array passing a given truth test.
     *
     * @param  Function $callback   The function callback for the truth test, the function
     *                              takes in two arguments, the key and the value.
     * @return Object               The first object that parses the truth test.
     */
     function array_first($array, $callback)
     {
         return Arrays::first($array, $callback);
     }
}

if (!function_exists('array_last')) {

     /**
     * The last method returns the last element of an array passing a given truth test.
     *
     * @param  Function $callback   The function callback for the truth test, the function
     *                              takes in two arguments, the key and the value.
     * @return Object               The last object that parses the truth test.
     */
     function array_last($array, $callback)
     {
         return Arrays::last($array, $callback);
     }
}

if (!function_exists('array_flatten')) {

     /**
     * The flatten method will flatten a multi-dimensional array into a single level.
     *
     * @return Array The new flattened array.
     */
    function array_flatten($array)
    {
        return Arrays::flatten($array);
    }
}
