<?php

return [
	// Requests
	'GET' => 'Input/Input',
	'POST' => 'Input/Input',

	// Helpers
	'String' 	=> 'Helpers/String/String',
	'Arrays' 	=> 'Helpers/Arrays/Arrays',

	// Database
	'Eloquent' 	=> 'Database/Database',
];