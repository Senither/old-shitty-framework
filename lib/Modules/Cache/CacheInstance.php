<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'CacheInterface.php';
require_once 'CacheException.php';

class CacheInstance
{
    private $client = null;

    public function __construct($type, $path)
    {
        switch (mb_strtolower($type)) {
            default:
                if (!class_exists('FileCacheHandler')) {
                    require_once 'Handler/FileCacheHandler.php';
                }
                $this->client = new FileCacheHandler($path);
                break;
        }
    }

    /**
     * Puts something into the cache with the given name(key), value and time.
     *
     * @param  String $name   The name(Key) to save the cache under.
     * @param  Object $value  The object to save in the cache.
     * @param  Carbon $carbon A Carbon object to determ the amount of time the object should be saved for.
     * @return null|bool      Depending on the client, this method may return different things.
     */
    public function put($name, $value, Carbon $carbon)
    {
        if ($this->client != null) {
            return $this->client->put(utf8_decode($name), $value, $carbon->getTimestamp());
        }
        throw new CacheException('Client has not been initialized, failed to call \'put\' with the given values.', 7);
    }

    /**
     * Gets something from the cache
     *
     * @param  String $name     The name(Key) to get the cache from
     * @param  Object $fallback A fallback object, in-case the cache isn't found this will be returned instead.
     *                          You can parse in a function as a fallback if you want a callback instead.
     * @return Object           The cache or the fallback object.
     */
    public function get($name, $fallback = null)
    {
        if ($this->client != null) {
            return $this->client->get(utf8_decode($name), $fallback);
        }
        throw new CacheException('Client has not been initialized, failed to call \'get\' with the given values.', 7);
    }

    public function pull($name, $fallback = null)
    {
        if ($this->client != null) {
            return $this->client->pull(utf8_decode($name), $fallback);
        }
        throw new CacheException('Client has not been initialized, failed to call \'pull\' with the given values.', 7);
    }

    public function push($name, $value)
    {
        if ($this->client != null) {
            return $this->client->push(utf8_decode($name), $value);
        }
        throw new CacheException('Client has not been initialized, failed to call \'push\' with the given values.', 7);
    }

    public function has($name)
    {
        if ($this->client != null) {
            return $this->client->has(utf8_decode($name));
        }
        throw new CacheException('Client has not been initialized, failed to call \'has\' with the given values.', 7);
    }

    public function forget($name)
    {
        if ($this->client != null) {
            return $this->client->forget(utf8_decode($name));
        }
        throw new CacheException('Client has not been initialized, failed to call \'forget\' with the given values.', 7);
    }

    public function forever($name)
    {
        if ($this->client != null) {
            return $this->client->forever(utf8_decode($name));
        }
        throw new CacheException('Client has not been initialized, failed to call \'forever\' with the given values.', 7);
    }

    public function extend($name, Carbon $carbon)
    {
        if ($this->client != null) {
            return $this->client->extend(utf8_decode($name), $carbon->getTimestamp());
        }
        throw new CacheException('Client has not been initialized, failed to call \'extend\' with the given values.', 7);
    }

    public function flush()
    {
        if ($this->client != null) {
            return $this->client->flush();
        }
        throw new CacheException('Client has not been initialized, failed to call \'flush\' with the given values.', 7);
    }

    public function all()
    {
        if ($this->client != null) {
            return $this->client->all();
        }
        throw new CacheException('Client has not been initialized, failed to call \'all\' with the given values.', 7);
    }
}
