<?php
defined('LIB_START') or exit('No direct script access allowed');

interface CacheInterface extends LombokProvider
{
    public function pull($name, $fallback);

    public function push($name, $value);

    public function has($name);

    public function forget($name);

    public function forever($name);

    public function extend($name, $carbon);

    public function flush();

    public function all();
}
