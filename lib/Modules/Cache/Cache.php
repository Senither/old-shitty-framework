<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'CacheInterface.php';
require_once 'CacheException.php';

class Cache extends ModuleProvider
{
    private static $client = null;

    public function __construct()
    {
        switch (mb_strtolower(Config::get('cache.default', 'file'))) {
            default:
                if (!class_exists('FileCacheHandler')) {
                    require_once 'Handler/FileCacheHandler.php';
                }
                self::$client = new FileCacheHandler;
                break;
        }
    }

    public static function loadInstance($type, $path)
    {
        if (!class_exists('CacheInstance')) {
            require_once 'CacheInstance.php';
        }
        return new CacheInstance($type, $path);
    }

    /**
     * Puts something into the cache with the given name(key), value and time.
     *
     * @param  String $name   The name(Key) to save the cache under.
     * @param  Object $value  The object to save in the cache.
     * @param  Carbon $carbon A Carbon object to determ the amount of time the object should be saved for.
     * @return null|bool      Depending on the client, this method may return different things.
     */
    public static function put($name, $value, Carbon $carbon)
    {
        if (self::$client != null) {
            return self::$client->put($name, $value, $carbon->getTimestamp());
        }
        throw new CacheException('Client has not been initialized, failed to call \'put\' with the given values.', 7);
    }

    /**
     * Gets something from the cache
     *
     * @param  String $name     The name(Key) to get the cache from
     * @param  Object $fallback A fallback object, in-case the cache isn't found this will be returned instead.
     *                          You can parse in a function as a fallback if you want a callback instead.
     * @return Object           The cache or the fallback object.
     */
    public static function get($name, $fallback = null)
    {
        if (self::$client != null) {
            return self::$client->get($name, $fallback);
        }
        throw new CacheException('Client has not been initialized, failed to call \'get\' with the given values.', 7);
    }

    public static function pull($name, $fallback = null)
    {
        if (self::$client != null) {
            return self::$client->pull($name, $fallback);
        }
        throw new CacheException('Client has not been initialized, failed to call \'pull\' with the given values.', 7);
    }

    public static function push($name, $value)
    {
        if (self::$client != null) {
            return self::$client->push($name, $value);
        }
        throw new CacheException('Client has not been initialized, failed to call \'push\' with the given values.', 7);
    }

    public static function has($name)
    {
        if (self::$client != null) {
            return self::$client->has($name);
        }
        throw new CacheException('Client has not been initialized, failed to call \'has\' with the given values.', 7);
    }

    public static function forget($name)
    {
        if (self::$client != null) {
            return self::$client->forget($name);
        }
        throw new CacheException('Client has not been initialized, failed to call \'forget\' with the given values.', 7);
    }

    public static function forever($name)
    {
        if (self::$client != null) {
            return self::$client->forever($name);
        }
        throw new CacheException('Client has not been initialized, failed to call \'forever\' with the given values.', 7);
    }

    public static function extend($name, Carbon $carbon)
    {
        if (self::$client != null) {
            return self::$client->extend($name, $carbon->getTimestamp());
        }
        throw new CacheException('Client has not been initialized, failed to call \'extend\' with the given values.', 7);
    }

    public static function flush()
    {
        if (self::$client != null) {
            return self::$client->flush();
        }
        throw new CacheException('Client has not been initialized, failed to call \'flush\' with the given values.', 7);
    }

    public static function all()
    {
        if (self::$client != null) {
            return self::$client->all();
        }
        throw new CacheException('Client has not been initialized, failed to call \'all\' with the given values.', 7);
    }
}
