<?php
defined('LIB_START') or exit('No direct script access allowed');

class DatabaseObject
{
    public $data;
    public $_backup;
    public $_changes = false;
    // public $_created = true;

    public $_key;
    public $_name;
    public $_instance;

    public function __construct(Eloquent $instance, $arr, $name, $key, $created = true)
    {
        $this->data = $arr;
        $this->_backup = $arr;

        $this->_key = $key;
        $this->_name = $name;
        // $this->_created = $created;
        $this->_instance = $instance;
    }

    public function __destruct()
    {
        if ($this->_changes) {
            Eloquent::update($this);
        }
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        throw new InvalidArgumentException(" \"{$name}\" was not found under the Database Object class");
    }

    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->data)) {
            $this->data[$name] = $value;

            if ($this->_backup[$name] == $value) {
                return true;
            }
            $this->_changes = true;
            return true;
        }
        throw new InvalidArgumentException(" \"{$name}\" was not found under the Database Object class");
    }
    
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __debugInfo()
    {
        return $this->data;
    }

    public function toArray()
    {
        // return $this;
        return [
            'fields'    => $this->data,
            'settings'  => [
                'changes'    => $this->_changes,
                'key'        => $this->_key,
                'name'        => $this->_name
            ]
        ];
    }
}
