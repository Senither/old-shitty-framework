<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'DatabaseType.php';
require_once 'DatabaseObject.php';
require_once 'Eloquent.php';
require_once 'Schema.php';

/**
 * PDO SQL Class, PDO made easy.
 *
 * @author Alexis / Senither.
 * @version 1.0
 * @link http://senither.com/
 *
 */
class Database extends ModuleProvider
{

    // Database types
    private static $DatebaseTypes = [DatabaseType::MySQL, DatabaseType::MsSQL, DatabaseType::SyBase, DatabaseType::SQLLite];
    // PDO Connector variables.
    private static $dbh;
    private static $error;
    // Query statement.
    private static $stmt;

    // Boolean to see if we're connected to the database
    private static $connection = false;
    private static $connectionTries = 0;

    // Search eloquent
    private static $eloquents = [];

    // queries
    private static $queries = 0;

    // Database Schema instance
    private static $schema = null;

    public function __construct()
    {
        $this->__loadModule();
    }

    public function __loadModule()
    {
        $type = Config::get('database.default', 'mysql');
        $data = Config::get('database.connections.' . $type);

        if ($data == null || !is_array($data)) {
            throw new RuntimeException("Failed to load configuration information for the database connection");
        }

        if (!isset($data['driver'])) {
            throw new RuntimeException("'driver' key was not found in the \$data array, can't connect to a database without a type.");
        }

        $dns = self::formatConnectionString($data['driver'], $data['host'], $data['username'], $data['password'], $data['database']);

        if (!self::connect($type, $dns, $data)) {
            throw new Exception("Failed to connect to the database on the given protocol({$type})");
        }
    }

    public static function isConnected()
    {
        if (!self::$connection && self::$connectionTries < 3) {
            new self;
        }
        return self::$connection;
    }

    public static function connect($type, $dns, $arr)
    {
        try {
            if ($type == DatabaseType::MySQL) {
                self::$dbh = new PDO($dns, $arr['username'], $arr['password']);
            } elseif ($type == DatabaseType::MsSQL || $type == DatabaseType::SyBase) {
                $dns .= ', ' . $arr['username'] . ', ' . $arr['password'];
                self::$dbh = new PDO($dns);
            }

            self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$dbh->setAttribute(PDO::ATTR_PERSISTENT, true);

            self::$connection = true;

            return true;
        } catch (PDOException $ex) {
            if (ENVIRONMENT == 'development') {
                throw new RuntimeException($ex->getMessage(), $ex->getCode());
            } else {
                die($ex->getMessage());
            }
        }
        return false;
    }

    private static function formatConnectionString($type, $host, $user, $pass, $db)
    {
        switch ($type) {
            case 'mysql':
                return $type . ':host=' . $host . ';dbname=' . $db;
            case 'mssql':
            case 'sybase':
                return $type . ':host=' . $host . ';dbname=' . $db . ', ' . $user . ', ' . $pass;

            case 'sqlite':
                return 'sqlite:' . $db;
        }
    }

    private static function isValidDatabaseType($type)
    {
        foreach (self::$DatebaseTypes as $index => $value) {
            if ($type == $value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates/Instantiate our SQL query.
     *
     * @param query SQL Query.
     */
    public static function query($query)
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        self::$stmt = self::$dbh->prepare($query);
    }

    /**
     * Binds values to our query.
     *
     * @param param Placeholder value for the SQL statement.
     * @param value Bind value for the placeholder.
     * @param type Datatype of the parameter, this is null by default.
     */
    public static function bind($param, $value, $type = null)
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
                    break;
            }
        }
        self::$stmt->bindValue($param, $value, $type);
    }

    /**
    * Binds an array of values to our query.
    *
    * @param array Uses an array for our placeholder values.
    */
    public static function bindArray($array)
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        foreach ($array as $param => $value) {
            self::bind($param, $value);
        }
    }

    /**
     * Execute the SQL query.
     */
    public static function execute()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        self::$queries++;
        return self::$stmt->execute();
    }

    /**
     * Get the result set from the SQL query statement.
     *
     * @return DataObject array.
     */
    public static function resultSet()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        self::execute();
        return self::$stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Gets a single row from the SQL query statement.
     *
     * @return DataObject array.
     */
    public static function single()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        self::execute();
        return self::$stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Gets the amount of rows returned by our SQL statement.
     *
     * @return int Row count.
     */
    public static function rowCount()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        return self::$stmt->rowCount();
    }

    /**
     * Gets the ID from our last query.
     *
     * @return int Query id.
     */
    public static function lastInsertId()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        return self::$dbh->lastInsertId();
    }

    /**
     * Starts the transaction.
     */
    public static function startTransaction()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        return self::$dbh->beginTransaction();
    }

    /**
     * Ends the transaction.
     */
    public static function endTransaction()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        return self::$dbh->commit();
    }

    /**
     * Cancel the current transaction.
     */
    public static function cancelTransaction()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        return self::$dbh->rollBack();
    }

    /**
     * Debugs the SQL query by dumping the prepared statements from the bind method.
     */
    public static function debugParams()
    {
        if (!self::isConnected()) {
            throw new RuntimeException("Can not run a PDO method without a open database connection!");
        }
        return self::$stmt->debugDumpParams();
    }

    /**
     * Returns the amount of queries executed on the current page.
     * @return int The amount of queries that has be ran of the current page.
     */
    public static function getQueries()
    {
        return self::$queries;
    }

    /**
     * Creates a eloquent object based off the template file if it's found
     * @param  String|Eloquent   $eloquent A eloquent object name
     * @return Eloquent                    The eloquent object
     */
    public static function eloquent($eloquent)
    {
        if ($eloquent instanceof ELoquent) {
            return Eloquent::load($eloquent);
        }

        if (!isset(self::$eloquents[$eloquent])) {
            $path = lib_path() . '/Modules/Database/Eloquent/' . $eloquent . '.php';
            if (file_exists($path)) {
                require_once $path;
            }
        }

        $class = new $eloquent;

        if ($class instanceof Eloquent) {
            return Eloquent::load($class);
        }
    }

    public static function schema()
    {
        if (self::$schema == null) {
            self::$schema = new Schema;
        }
        return self::$schema;
    }
}
