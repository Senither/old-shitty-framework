<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'EloquentArray.php';

abstract class Eloquent
{
    private static $eloquents    = [];
    private static $latest        = null;

    public static function load(Eloquent $instance)
    {
        $name = get_class($instance);

        $table = null;
        $select = [];
        foreach (get_object_vars($instance) as $var => $value) {
            if ($var == 'table') {
                $table = $value;
            } else {
                $select[] .= $var;
            }
        }

        if (!isset(self::$eloquents[$name])) {
            self::$eloquents[$name] = [
                'name' => $name,
                'table' => $table,
                'query' => null,
                'class' => $instance,
                'calls' => []
            ];
        }

        if ($table == null) {
            throw new RuntimeException("Table can not be null for a Eloquent object!");
        }

        if (!empty($select)) {
            $select = '`' . implode('`, `', $select) . '`';
        }

        self::$eloquents[$name]['query'] = "SELECT {$select} FROM `{$table}`";
        self::$latest = $name;

        return $instance;
    }

    public static function update(DatabaseObject $object)
    {
        $arr = get_object_vars($object);

        $match = $arr['_backup'];
        $fields = [];

        $eloquent = self::$eloquents[$arr['_name']];
        $query    = null;

        $method = $eloquent['calls'][key($eloquent['calls'])]['method'];

        switch ($method) {
            case 'pull':
                return;

            case 'create':
                $query = "INSERT INTO `{$eloquent['table']}` SET ";

                foreach ($arr['data'] as $name => $value) {
                    $fields[$name] = $value;
                    $query .="`{$name}` = :{$name}, ";
                }
                break;

            default:
                $query = "UPDATE `{$eloquent['table']}` SET ";

                foreach ($arr['data'] as $name => $value) {
                    if ($match[$name] != $value) {
                        $fields[$name] = $value;
                        $query .= "`{$name}` = :{$name}, ";
                    }
                }
                break;
        }

        $query = mb_substr($query, 0, (mb_strlen($query) - 2));
        $call = $eloquent['calls'][$arr['_key']];
        $whereFields = [];

        switch ($call['method']) {
            case 'find':
                $query .= " WHERE `{$call['field']}` = :{$call['field']}";
                $whereFields[$call['field']] = $call['value'];
                break;
        }

        $identifier = $call['field'];
        if ($call['method'] == 'create' && isset($fields[$identifier])) {
            Database::query("SELECT `{$identifier}` FROM `{$eloquent['table']}` WHERE `{$identifier}` = :value");
            Database::bind('value', $fields[$identifier]);

            if (Database::single() !== false) {
                return false;
            }
        }
        Database::query($query);
        Database::bindArray($fields);

        if (!empty($whereFields)) {
            Database::bindArray($whereFields);
        }

        return Database::execute();
    }

    public function pull($where, $identifier = 'id')
    {
        $ea = new EloquentArray;

        if (!is_array($where)) {
            return $ea;
        }

        $increment = 0;

        $whereClause = ' WHERE ';
        $bindClause  = array();
        foreach ($where as $field => $item) {
            $bind          = $field . $increment++;
            $whereClause .= "`{$field}` = :{$bind} AND ";

            $bindClause[$bind] = $item;
        }
        $whereClause = rtrim($whereClause, ' AND ');

        Database::query(self::$eloquents[self::$latest]['query'] . $whereClause . ' ORDER BY `'.$identifier.'` ASC');
        Database::bindArray($bindClause);

        $result = Database::resultSet();

        if ($result == null) {
            return $ea;
        }

        foreach ($result as $index => $arr) {
            $key = $this->generateKey();
            self::$eloquents[self::$latest]['calls'][$key] = [
                'method' => 'pull',
                'field' => $identifier,
                'value' => $arr[$identifier]
            ];

            $ea->addItem(new DatabaseObject($this, $arr, self::$latest, $key));
        }

        Database::query("DELETE FROM `" . self::$eloquents[self::$latest]['table'] . "`{$whereClause};");
        Database::bindArray($bindClause);
        Database::execute();

        return $ea;
    }

    public function find($where, $identifier = 'id')
    {
        $ea = new EloquentArray;

        if (!is_array($where)) {
            return $ea;
        }

        $increment = 0;

        $whereClause = ' WHERE ';
        $bindClause  = array();
        foreach ($where as $field => $item) {
            $bind          = $field . $increment++;
            $whereClause .= "`{$field}` = :{$bind} AND ";

            $bindClause[$bind] = $item;
        }

        Database::query(self::$eloquents[self::$latest]['query'] . rtrim($whereClause, ' AND ') . ' ORDER BY `'.$identifier.'` ASC');
        Database::bindArray($bindClause);

        $result = Database::resultSet();

        if ($result == null) {
            return $ea;
        }

        foreach ($result as $index => $arr) {
            $key = $this->generateKey();
            self::$eloquents[self::$latest]['calls'][$key] = [
                'method' => 'find',
                'field' => $identifier,
                'value' => $arr[$identifier]
            ];

            $ea->addItem(new DatabaseObject($this, $arr, self::$latest, $key));
        }

        return $ea;
    }

    public function all($identifier = 'id', $order = [], $limit = -1, $offset = 0)
    {
        $ea = new EloquentArray;

        $orderQuery = '';
        if (!empty($order)) {
            $orderType = (isset($order[1])) ? $order[1] : 'ASC';
            $orderQuery .= " ORDER BY `$order[0]` {$orderType}";
        }

        $query = self::$eloquents[self::$latest]['query'] . $orderQuery . (($limit != -1) ? " LIMIT {$limit} OFFSET {$offset}" : '');
        Database::query($query);

        $result = Database::resultSet();
        if ($result == null) {
            return $ea;
        }

        foreach ($result as $index => $arr) {
            $key = $this->generateKey();
            self::$eloquents[self::$latest]['calls'][$key] = [
                'method' => 'all',
                'field' => $identifier,
                'value' => $arr[$identifier]
            ];

            $ea->addItem(new DatabaseObject($this, $arr, self::$latest, $key));
        }

        return $ea;
    }

    public function create($callback = null, $identifier = 'id')
    {
        $ea = new EloquentArray;

        if (!is_callable($callback) && $callback != null) {
            return $ea;
        }

        $fields = [];
        $fieldsArray = [];
        foreach (get_object_vars($this) as $field => $placeholder) {
            if ($field != 'table') {
                $fields[$field] = $placeholder;
                $fieldsArray[] .= $field;
            }
        }

        $key    = $this->generateKey();
        $object = new DatabaseObject($this, $fields, self::$latest, $key, false);

        foreach ($fields as $name => $property) {
            if ($property != null) {
                $object->_changes = true;
            }
            $object->{$name} = $property;
        }

        if (is_callable($callback)) {
            call_user_func_array($callback, [&$object]);
        }

        if ($object != null && ($object instanceof DatabaseObject)) {
            self::$eloquents[self::$latest]['calls'][$key] = [
                'method' => 'create',
                'field'  => $identifier,
                'value'  => null
            ];

            $ea->addItem($object);
        }

        return $ea;
    }

    private function generateKey()
    {
        $key = '';
        $azl = range('a', 'z');
        $azu = range('A', 'Z');
        $num = range('0', '9');

        for ($i = 0; $i < 6; $i++) {
            for ($x = 0; $x < 5; $x++) {
                switch (rand(0, 2)) {
                    case 0:
                        $key .= $azl[array_rand($azl)];
                        break;

                    case 1:
                        $key .= $azu[array_rand($azu)];
                        break;

                    case 2:
                        $key .= $num[array_rand($num)];
                        break;
                }
            }
            $key .= '-';
        }
        return rtrim($key, '-');
    }
}
