<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'Schema/BluePrint.php';
require_once 'Schema/Fluent.php';

class Schema
{
    public function create($table, $callback)
    {
        return new BluePrint($table, $callback);
    }

    // alter table `users` rename to `users_dev`
    public function rename($from, $to)
    {
        Database::query("ALTER TABLE IF EXISTS `{$from}` RENAME TO `{$to}`;");
    }
}
