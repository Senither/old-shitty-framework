<?php
defined('LIB_START') or exit('No direct script access allowed');

/**
 * DatabaseType enums for the PDO Database class.
 */
class DatabaseType
{
    const MySQL = 'mysql';
    const MsSQL = 'mssql';
    const SyBase = 'sybase';
    const SQLLite = 'sqlite';
}
