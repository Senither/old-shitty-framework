<?php
defined('LIB_START') or exit('No direct script access allowed');

class EloquentArray implements Iterator, Countable
{
    private $properties = [];

    public function __construct(array $objs = [])
    {
        $this->properties = $objs;
    }

    public function addItem(DatabaseObject $obj)
    {
        $this->properties[] = $obj;
    }

    public function removeItem($index)
    {
        if (isset($this->properties[$index])) {
            unset($this->properties[$index]);
        }
    }

    public function __get($name)
    {
        if (empty($this->properties)) {
            return null;
        }
        return $this->properties[0]->{$name};
    }

    public function __set($name, $value)
    {
        if (empty($this->properties)) {
            return;
        }
        $this->properties[0]->{$name} = $value;
    }

    // Count method

    public function count()
    {
        return count($this->properties);
    }

    // Iterator methods

    public function rewind()
    {
        reset($this->properties);
    }

    public function current()
    {
        return current($this->properties);
    }

    public function key()
    {
        return key($this->properties);
    }

    public function next()
    {
        return next($this->properties);
    }

    public function valid()
    {
        $key = key($this->properties);
        return ($key !== null && $key !== false);
    }

    public function isEmpty()
    {
        return empty($this->properties);
    }

    public function toArray()
    {
        if ($this->isEmpty()) {
            return [];
        }

        $set = [];
        foreach ($this->properties as $obj) {
            $set[] = $obj->toArray();
        }

        return $set;
    }

    public function asArray()
    {
    }
}
