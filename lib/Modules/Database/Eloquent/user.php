<?php
defined('LIB_START') or exit('No direct script access allowed');

class user extends Eloquent
{

    // The table to get the information from
    protected $table = 'users';

    // Fields
    protected $id;
    protected $username;
    protected $password;
    protected $name;
}
