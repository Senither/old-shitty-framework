<?php
defined('LIB_START') or exit('No direct script access allowed');

class ConfigObject
{
    private $file;
    private $properties = [];
    private $changes    = false;

    public function __construct($file, array $data)
    {
        $this->file      = $file;
        $this->properties = $data;

        $this->properties['connections']['sqlite']['driver'] = 'test';
    }

    public function __destruct()
    {
        if ($this->changes) {
            if (!class_exists('StringParserProvider')) {
                Autoloader::forceLoad('StringParserProvider');
            }

            if (!class_exists('Arrays')) {
                Autoloader::forceLoad('Arrays');
            }

            $rw = new ConfigRewrite;
            $rw->toFile(root_path() . "/Config/{$this->file}", Arrays::dot($this->properties));
        }
    }

    public function put($name, $value)
    {
        $res =& $this->properties;

        while (count($name) > 1) {
            $next = array_shift($name);

            if (! isset($res[$next]) || ! is_array($res[$next])) {
                $res[$next] = [];
            }

            $res =& $res[$next];
        }

        $res[array_shift($name)] = $value;
        $this->changes = true;
    }

    public function find($name)
    {
        $result = $this->properties;
        foreach ($name as $item) {
            if (isset($result[$item])) {
                $result = $result[$item];
            } else {
                return null;
            }
        }
        return $result;
    }
}
