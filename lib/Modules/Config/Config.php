<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'ConfigObject.php';
require_once 'ConfigRewrite.php';

class Config extends ModuleProvider
{
    private static $configs = array();

    public static function put($name, $value)
    {
        $path = explode('.', $name);
        $file = mb_strtolower($path[0]) . '.php';

        unset($path[0]);
        $path = array_values($path);

        if (!isset(self::$configs[$file]) && file_exists(root_path() . "/Config/{$file}")) {
            self::$configs[$file] = new ConfigObject($file, require_once root_path() . "/Config/{$file}");
        }

        if (!isset(self::$configs[$file])) {
            return false;
        }

        return self::$configs[$file]->put($path, $value);
    }

    public static function get($name, $fallback = null)
    {
        $path = explode('.', $name);
        $file = mb_strtolower($path[0]) . '.php';

        unset($path[0]);
        $path = array_values($path);

        if (!isset(self::$configs[$file]) && file_exists(root_path() . "/Config/{$file}")) {
            self::$configs[$file] = new ConfigObject($file, require_once root_path() . "/Config/{$file}");
        }

        if (!isset(self::$configs[$file])) {
            return $fallback;
        }

        $res = self::$configs[$file]->find($path);
        if ($res === null) {
            return $fallback;
        }

        return $res;
    }
}
