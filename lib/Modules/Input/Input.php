<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'Handler/GET.php';
require_once 'Handler/POST.php';

class Input extends ModuleProvider implements RequestProvider
{
    public function __construct()
    {
        // We need the constructor when we're calling for a static method dirictly
    }

    public static function get($name, $fallback = null)
    {
        if (($result = GET::get($name)) != null) {
            return $result;
        } elseif (($result = POST::get($name)) != null) {
            return $result;
        }
        return $fallback;
    }

    public static function has($name)
    {
        foreach (func_get_args() as $arg) {
            if (GET::has($name) || POST::has($name)) {
                return true;
            }
        }
        return false;
    }

    public static function all()
    {
        return [
            'get' => GET::all(),
            'post' => POST::all()
        ];
    }
}
