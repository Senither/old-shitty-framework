<?php
defined('LIB_START') or exit('No direct script access allowed');

class GET extends ModuleProvider implements RequestProvider
{
    public function __construct()
    {
        // We need the constructor when we're calling for a static method dirictly
    }

    public static function get($name, $fallback = null)
    {
        $parsed = explode('.', $name);
        $result = $_GET;

        while ($parsed) {
            $next = array_shift($parsed);

            if (isset($result[$next])) {
                $result = $result[$next];
            } else {
                if (is_callable($fallback)) {
                    return call_user_func_array($fallback, []);
                } else {
                    return $fallback;
                }
            }
        }

        return $result;
    }

    public static function has($name)
    {
        foreach (func_get_args() as $arg) {
            $parsed = explode('.', $arg);
            $result = $_GET;

            while ($parsed) {
                $next = array_shift($parsed);

                if (isset($result[$next])) {
                    $result = $result[$next];
                } else {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static function all()
    {
        return $_GET;
    }
}
