<?php
defined('LIB_START') or exit('No direct script access allowed');

class Cookie extends ModuleProvider
{
    private static $token;

    public function __construct()
    {
        self::$token = base64_encode($_SERVER['SERVER_NAME']);
    }

    public static function put($name, $value, Carbon $carbon)
    {
        $parsed = explode('.', $name);
        $name   = self::encode($parsed[0]);
        $value  = self::encode(json_encode($value));

        $_COOKIE[$name] = $value;
        setcookie($name, $value, $carbon->getTimestamp(), '/', null);
    }

    public static function push($name, $value)
    {
    }

    public static function get($name, $fallback = null)
    {
        $parsed = explode('.', $name);
        $name   = self::encode($parsed[0]);

        if (!isset($_COOKIE[$name])) {
            return null;
        }

        $result = json_decode(self::decode($_COOKIE[$name]), true);

        for ($i = 1; $i < count($parsed); $i++) {
            $next = $parsed[$i];

            if (!isset($result[$next])) {
                $result[$next] = null;
            }

            $result = $result[$next];
        }

        return $result;
    }

    public static function has($name)
    {
        $parsed = explode('.', $name);
        $name   = self::encode($parsed[0]);

        if (!isset($_COOKIE[$name])) {
            return null;
        }

        $result = json_decode(self::decode($_COOKIE[$name]), true);

        for ($i = 1; $i < count($parsed); $i++) {
            $next = $parsed[$i];

            if (!isset($result[$next])) {
                return false;
            }

            $result = $result[$next];
        }

        return true;
    }

    public static function forget($name, $encode = true)
    {
        if ($encode) {
            $name = self::encode($name);
        }

        if (!isset($_COOKIE[$name])) {
            return false;
        }

        unset($_COOKIE[$name]);
        setcookie($name, null, -1, '/');
        return true;
    }

    public static function flush()
    {
        foreach ($_COOKIE as $name => $value) {
            if ($name == 'PHPSESSID' || $name == Config::get('session.cookie')) {
                continue;
            }

            self::forget($name, false);
        }
    }

    public static function all()
    {
        $cookies = [];

        foreach ($_COOKIE as $name => $value) {
            if ($name == 'PHPSESSID' || $name == Config::get('session.cookie')) {
                continue;
            }

            $cookies[self::decode($name)] = json_decode(self::decode($_COOKIE[$name]), true);
        }

        return $cookies;
    }

    private static function encode($value)
    {
        $token  = base64_encode(gzcompress(serialize(self::$token . $value)));
        $length = mb_strlen($token);
        $remove = 0;

        for ($i = 0; $i < $length; $i++) {
            if ($token[$i] == '=') {
                $remove++;
            }
        }

        return trim($token, '=') . ':' . $remove;
    }

    private static function decode($value)
    {
        $split = explode(':', $value);
        $key = $split[0];

        for ($i = 0; $i < ((int) $split[1]); $i++) {
            $key .= '+';
        }

        return trim(unserialize(gzuncompress(base64_decode($key))), self::$token);
    }
}
