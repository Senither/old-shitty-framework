<?php
defined('LIB_START') or exit('No direct script access allowed');

class Minimize extends ModuleProvider
{
    public static function enable()
    {
        self::setStatus(true);
    }

    public static function disable()
    {
        self::setStatus(false);
    }

    public static function isEnabled()
    {
        return self::getStatus();
    }

    public static function isDisabled()
    {
        return !self::getStatus();
    }

    private static function setStatus($status)
    {
        $_SERVER['MINIMIZE'] = $status;
    }

    private static function getStatus()
    {
        return $_SERVER['MINIMIZE'];
    }
}
