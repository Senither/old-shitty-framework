<?php
defined('LIB_START') or exit('No direct script access allowed');

class csrf extends ModuleProvider
{
    public static function validate($tokenName = 'csrf_token')
    {
        if (Session::has('csrf_token') && Input::has($tokenName)) {
            return Session::get('csrf_token') == Input::get($tokenName);
        }
        return false;
    }

    public static function field($tokenName = 'csrf_token')
    {
        return '<input type="hidden" name="' . $tokenName . '" value="'. self::make() .'">';
    }

    public static function make()
    {
        if (function_exists("hash_algos") && in_array("sha512", hash_algos())) {
            $token = hash("sha512", mt_rand(0, mt_getrandmax()));
        } else {
            $token = ' ';
            for ($i = 0; $i < 128; $i++) {
                $r = mt_rand(0, 35);
                if ($r < 26) {
                    $c = chr(ord('a') + $r);
                } else {
                    $c = chr(ord('0') + $r - 26);
                }
                $token .= $c;
            }
        }
        Session::put('csrf_token', $token);

        return $token;
    }

    public static function forget($tokenName = 'csrf_token')
    {
        Session::forget($tokenName);
    }
}
