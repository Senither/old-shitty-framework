<?php

require_once 'Run.php';

// Handlers
require_once 'Handler/HandlerInterface.php';
require_once 'Handler/Handler.php';
require_once 'Handler/PrettyPageHandler.php';
require_once 'Handler/JsonResponseHandler.php';

// Exceptions
require_once 'Exception/ErrorException.php';
require_once 'Exception/Formatter.php';
require_once 'Exception/Inspector.php';
require_once 'Exception/Frame.php';
require_once 'Exception/FrameCollection.php';

// Utils
require_once 'Util/TemplateHelper.php';
require_once 'Util/Misc.php';

$run     	 = new \Whoops\Run;
$handler 	 = new \Whoops\Handler\PrettyPageHandler;
$JsonHandler = new \Whoops\Handler\JsonResponseHandler;
 
$run->pushHandler($JsonHandler);
$run->pushHandler($handler);
$run->register();