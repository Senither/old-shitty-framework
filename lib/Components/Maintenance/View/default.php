<html>
	<head>
		<title>503 - Maintenance</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
				background-color: #212326;
			}
			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}
			.content {
				text-align: center;
				display: inline-block;
			}
			.title {
				font-size: 64px;
				margin-bottom: 40px;
				font-weight: 300;
			}
			.subtext {
				font-size: 24px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">Sorry, we're down for maintenance</div>
				<div class="subtext">Error Code: 503
				<br>IP Address: {{IP}}</div>
			</div>
		</div>
	</body>
</html>