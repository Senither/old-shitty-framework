<?php

class DefaultMaintenanceHandler extends MaintenanceFactory
{

	public function handel()
	{
		$this->load('custom');
	}

	public function finish()
	{
		$this->bind('path',   $_SERVER['REQUEST_URI']);
		$this->bind('home',   '/dashboard');
		echo $this->content;
	}
}
