<?php

require_once 'Maintenance.php';
require_once 'MaintenanceFactory.php';
require_once 'Handler/DefaultMaintenanceHandler.php';

$maintenance = new Maintenance;

$maintenance->sendHeader();
$maintenance->setHandler(new DefaultMaintenanceHandler);

$maintenance->build();
