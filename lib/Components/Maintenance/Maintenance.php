<?php

class Maintenance
{

	private $handler;

	public function sendHeader($code = 503)
	{
		switch ($code) {
			case 204: $text = 'No Content'; break;
			case 503: $text = 'Service Unavailable'; break;

			default:
				$text = 'Service Unavailable';
				$code = 503;
				break;
		}

		$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
		header($protocol . ' ' . $code . ' ' . $text);
		$GLOBALS['http_response_code'] = $code;
	}

	public function setHandler(MaintenanceFactory $handler)
	{
		$this->handler = $handler;
		$this->handler->handel();
	}

	public function build()
	{
		$this->handler->finish();
		exit(1);
	}
}
