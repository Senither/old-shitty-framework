<?php

abstract class MaintenanceFactory
{

	protected $content;

	public abstract function handel();

	public abstract function finish();

	public function load($view)
	{
		ob_start();
		echo file_get_contents(lib_path() . "/Components/Maintenance/View/{$view}.php");
		$this->content = ob_get_contents();
		ob_end_clean();

		if(isset($_SERVER['MINIMIZE']) || $_SERVER['MINIMIZE']) {
            $this->content = implode('', array_map("trim", explode("\n", $this->content) ));
        }
	}

	public function bind($key, $value)
	{
		$this->content = str_replace('{{' . mb_strtoupper($key) . '}}', $value, $this->content);
	}
}