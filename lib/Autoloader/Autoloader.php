<?php
defined('LIB_START') or exit('No direct script access allowed');

require_once 'AutoloaderInterface.php';
require_once dirname(__DIR__) . '/Providers/ModuleProvider.php';

class Autoloader extends ModuleProvider implements AutoloaderInterface
{
    private $path;
    private $kernel;

    private static $instance = null;
    private static $load = true;

    public function __construct()
    {
        ob_start();

        $this->path = lib_path();
        $this->kernel = require_once dirname(__DIR__) . '/Modules/Kernel.php';

        foreach ($this->kernel as $key => $value) {
            $this->kernel[mb_strtolower($key)] = $value;
        }

        if (MAINTENANCE && self::$load) {
            require_once dirname(__DIR__) . '/Components/Maintenance/init.php';
            self::$load = false;
        }
    }

    /**
     * Registers this instance as an autoloader.
     *
     * @param bool $prepend Whether to prepend the autoloader or not
     */
    public function register($prepend = false)
    {
        spl_autoload_register(array($this, 'load'), true, $prepend);
        register_shutdown_function(array($this, 'unregister'));
    }

    /**
     * Unregisters this instance as an autoloader.
     */
    public function unregister()
    {
        if (!isset($_SERVER['MINIMIZE']) || !$_SERVER['MINIMIZE']) {
            return;
        }

        $content = ob_get_contents();
        
        if (mb_strlen($content) > 0) {
            ob_end_clean();
        }
        echo implode('', array_map("trim", explode("\n", $content)));
    }

    /**
     * Loads the given class or provider.
     *
     * @param  string    $class The name of the class
     * @return bool|null True if loaded, null otherwise
     */
    public function load($class)
    {
        if (array_key_exists($class, $this->kernel)) {
            require_once "{$this->path}/Modules/{$this->kernel[$class]}.php";
            return true;
        }

        if (file_exists("{$this->path}/Modules/{$class}/{$class}.php")) {
            require_once "{$this->path}/Modules/{$class}/{$class}.php";

            $instance = new $class;

            if (($instance instanceof ModuleProvider)) {
                $instance->__loadModule();
                return true;
            }
        } elseif (file_exists("{$this->path}/Providers/{$class}.php")) {
            require_once "{$this->path}/Providers/{$class}.php";
            return true;
        }
    }

    /**
     * Autoloads the given modules from the boot file.
     *
     * @param  Array $functions The array from the boot file.
     * @return bool|null        True if loaded, null otherwise.
     */
    public function preload($functions)
    {
        if (empty($functions)) {
            return;
        }

        foreach ($functions as $class) {
            if (file_exists("{$this->path}/Modules/{$class}/{$class}.php")) {
                require_once "{$this->path}/Modules/{$class}/{$class}.php";

                $instance = new $class;

                if (($instance instanceof ModuleProvider)) {
                    $instance->__loadModule();
                    return true;
                }
            } elseif (file_exists("{$this->path}/Providers/{$class}.php")) {
                require_once "{$this->path}/Providers/{$class}.php";
                return true;
            }
        }
    }

    public static function forceLoad($class)
    {
        if (self::$instance == null) {
            self::$instance = new self;
        }
        self::$instance->load($class);
        // d(123);
    }
}
