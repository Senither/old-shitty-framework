<?php

/**
* Senither Library, developered by Alex Tan(Hjalte) because I was bored, and
* a bit for shits and giggles.
*
* @author Senither
* @link http://senither.com/
* @version 0.1
*/

/**
 * ---------------------------------------------------------------
 * APPLICATION START TIMER
 * ---------------------------------------------------------------
 *
 * This constant will be used later on to determen how fast the
 * application loaded everything.
 */
define('LIB_START', microtime(true));

/*
 * ---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 * ---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging, error reporting and exception handling.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 */
define('ENVIRONMENT', 'testing');

/*
 * ---------------------------------------------------------------
 * ERROR REPORTING
 * ---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and production will hide them.
 * Notice: By setting the enviorment to "maintenance", the whole system will show the
 * maintenance page, you can edit this at /lib/Errors/
 */
switch (ENVIRONMENT) {
    case 'development':
        error_reporting(-1);
        ini_set('display_errors', 1);
        require_once __DIR__  . '/Components/Whoops/Whoops.php';
        break;

    case 'testing':
        error_reporting(-1);
        ini_set('display_errors', 1);
        break;

    case 'production':
        error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
        ini_set('display_errors', 0);
        break;

    default:
        header('HTTP/1.1 503 Service Unavailable.', true, 503);
        echo 'The application environment is not set correctly.';
        exit(1); // EXIT_ERROR
}

/*
 * ---------------------------------------------------------------
 * Maintenance Mode
 * ---------------------------------------------------------------
 *
 * Determs if the site is in maintenance mode or not, if this is
 * set to true the maintenance component will run and end the page
 * before anything can run, avoiding silly errors and broken code.
 * If you want to edit the maintenance page you can do so at:
 *
 * Handler:     /lib/Components/Maintenance/Handler/
 * View:        /lib/Components/Maintenance/View/
 */
define('MAINTENANCE', false);

/*
 * ---------------------------------------------------------------
 * Minimize Mode
 * ---------------------------------------------------------------
 *
 * Converts all the html, css, javascript, etc on the pages that
 * are being loaded to be minimized as to help load the page
 * quicker, if you don't want the page to minimized you can use
 * the module to cancel the event.
 */
define('MINIMIZE', false);

/*
 * ---------------------------------------------------------------
 *  PRE-LOAD MODULES
 * ---------------------------------------------------------------
 *
 * Pre-Loads a list of modules before the rest of the page loads
 * to have them ready on the rest of the site, this is usefull
 * for modules like String and Arrays that include presidual
 * functions like "normal" PHP functions so there is multiple
 * of ways to call the static methods.
 * If you don't want anything to be pre-loaded just leave it as
 * an empty array.
 */
$preload = array(

);

/*
 * ---------------------------------------------------------------
 *  INCLUDE GLOBAL FUNCTIONS
 * ---------------------------------------------------------------
 *
 * Includes our global function list for the library so we can
 * use all of our fancy tools.
 */
require_once 'functions.php';

/*
 * ---------------------------------------------------------------
 *  INCLUDE AUTOLOADER
 * ---------------------------------------------------------------
 *
 * Includes our autoload handler to include all of our core moduels
 * and work models for us.
 */
require_once lib_path() . '/Autoloader/Autoloader.php';

/*
 * ---------------------------------------------------------------
 *  INSTANTIATE OUR AUTOLOADER
 * ---------------------------------------------------------------
 *
 * Creates an instance of our autoloader so we can get started.
 */
$load = new Autoloader();

/*
 * ---------------------------------------------------------------
 *  REGISTER THE AUTOLOADER
 * ---------------------------------------------------------------
 *
 * Registers our auto loader methods so we don't have to load all
 * the things in we need ourselfs, because we're lazy shits.
 */
$load->register(true);

/*
 * ---------------------------------------------------------------
 *  AUTOLOADS OUR MODULES
 * ---------------------------------------------------------------
 *
 * Auto loads our modules so they're ready for the rest of the page
 */
$load->preload($preload);

// Now we're done, if you need to change anything else it would
// be the configs but you need to go a folder back to find those.
