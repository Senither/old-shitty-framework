<?php
defined('LIB_START') or exit('No direct script access allowed');

interface LombokProvider
{
    public function put($name, $value, $carbon);

    public function get($name, $fallback = null);
}
