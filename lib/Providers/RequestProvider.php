<?php
defined('LIB_START') or exit('No direct script access allowed');

interface RequestProvider
{
    public static function get($name, $fallback = null);

    public static function has($name);
    
    public static function all();
}
